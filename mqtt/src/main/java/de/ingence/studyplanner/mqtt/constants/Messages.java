package de.ingence.studyplanner.mqtt.constants;

import de.ingence.studyplanner.core.constants.ConsoleStyles;



public class Messages extends ConsoleStyles {

    //FileSetup messages
    public static final String FILESETUP_PROPERTIESFILE_ALREADY_EXISTS = "Skipping creating <mqtt.properties> file. File already exists...";
    public static final String FILESETUP_PROPERTIESFILE_START_SETUP = "Creating <mqtt.properties> file...";
    public static final String FILESETUP_PROPERTIESFILE_CREATED = "File <mqtt.properties> created";
    public static final String FILESETUP_PROPERTIESFILE_INIT = "File <mqtt.properties> initializied";


}

