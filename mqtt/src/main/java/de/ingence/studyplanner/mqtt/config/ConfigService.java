package de.ingence.studyplanner.mqtt.config;

import de.ingence.studyplanner.mqtt.constants.MqttConstants;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.Properties;


public class ConfigService {

    public static String getConfig(String param) throws Exception {
        Properties p = new Properties();
        BufferedInputStream b = new BufferedInputStream(new FileInputStream(MqttConstants.CONFIG_DIR_PATH + "/" + MqttConstants.CONFIG_FILE));
        p.load(b);
        b.close();
        return p.getProperty(param);
    }

}
