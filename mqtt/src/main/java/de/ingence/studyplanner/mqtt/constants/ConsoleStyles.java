package de.ingence.studyplanner.mqtt.constants;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;


public class ConsoleStyles {

    //SOUT styling
    public static final String INFORMATION = "[Information] ";
    public static final String ERROR = "[Error] ";

    public static String curDate() {
        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String dateAsString = "[" + dateFormat.format(date) + "] ";
        return dateAsString;
    }

}