package de.ingence.studyplanner.mqtt.setup;

import de.ingence.studyplanner.mqtt.constants.Errors;
import de.ingence.studyplanner.mqtt.constants.Messages;
import de.ingence.studyplanner.mqtt.constants.MqttConstants;
import de.ingence.studyplanner.mqtt.constants.SetupConstants;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class ConfigSetupMqtt {

    public static boolean setup() throws Exception {
        //server.properties
        File propertiesFile = new File(MqttConstants.CONFIG_DIR_PATH + "/" + MqttConstants.CONFIG_FILE);
        if(propertiesFile.exists()) {
            System.out.println(Messages.curDate() + Messages.INFORMATION + Messages.FILESETUP_PROPERTIESFILE_ALREADY_EXISTS);
            return false;
        } else {
            File dir = new File(MqttConstants.CONFIG_DIR_PATH);
            if(!dir.exists()) {
                dir.mkdir();
            }
            System.out.println(Messages.curDate() + Messages.INFORMATION + Messages.FILESETUP_PROPERTIESFILE_START_SETUP);
            if(propertiesFile.createNewFile()) {
                System.out.println(Messages.curDate() + Messages.INFORMATION + Messages.FILESETUP_PROPERTIESFILE_CREATED);
                FileWriter fileWriter = new FileWriter(MqttConstants.CONFIG_DIR_PATH + "/" + MqttConstants.CONFIG_FILE);
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                bufferedWriter.write(SetupConstants.PROPERTIES_VALUES);
                bufferedWriter.close();
                System.out.println(Messages.curDate() + Messages.INFORMATION + Messages.FILESETUP_PROPERTIESFILE_INIT);
                return true;
            } else {
                throw new Exception(Errors.curDate() + Errors.ERROR + Errors.FILESETUP_PROPERTIESFILE_CREATION_FILED);
            }
        }

    }
}
