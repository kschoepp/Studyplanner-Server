package de.ingence.studyplanner.mqtt.constants;

public class MqttConstants {

    //Configuration default values
    //|---VERT.X default values
    public static final int MQTT_PORT = 1883;
    //File paths
    public static final String CONFIG_DIR_PATH = "./config";
    public static final String CONFIG_FILE = "mqtt.properties";

}
