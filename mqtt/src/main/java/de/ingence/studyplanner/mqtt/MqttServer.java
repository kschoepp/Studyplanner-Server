package de.ingence.studyplanner.mqtt;

import de.ingence.studyplanner.core.constants.AddressRegister;
import de.ingence.studyplanner.core.constants.CoreConstants;
import de.ingence.studyplanner.core.constants.HttpCodes;
import de.ingence.studyplanner.mqtt.config.ConfigService;
import de.ingence.studyplanner.mqtt.constants.MqttConstants;
import de.ingence.studyplanner.mqtt.setup.ConfigSetupMqtt;
import io.netty.handler.codec.mqtt.MqttConnectReturnCode;
import io.netty.handler.codec.mqtt.MqttQoS;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.buffer.Buffer;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonObject;
import io.vertx.mqtt.MqttEndpoint;
import io.vertx.mqtt.MqttServerOptions;
import io.vertx.mqtt.MqttTopicSubscription;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;


public class MqttServer extends AbstractVerticle {

    //Global variables
    private int port;
    private HashMap<String, MqttEndpoint> endpoints;
    //|---VERT.X Objects
    private io.vertx.mqtt.MqttServer mqttServer;
    private EventBus eventBus;

    public MqttServer() throws Exception {
        initPort();
        endpoints = new HashMap<>();
    }

    /**
     * Help function to read the MQTT-Port from configuration file
     * @throws Exception
     */
    private void initPort() throws Exception {
        String port = ConfigService.getConfig("port");
        if (port == null) {
            this.port = MqttConstants.MQTT_PORT;
        } else {
            this.port = Integer.parseInt(port);
        }
    }

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        //Options for MQTT-Server
        MqttServerOptions options = new MqttServerOptions();
        options.setPort(port);

        //Creating MQTT-Server
        this.mqttServer = io.vertx.mqtt.MqttServer.create(vertx, options);

        //Getting EventBus from VERT.X instance
        this.eventBus = vertx.eventBus();

        //================================
        //=== Define EventBus Handlers ===
        //================================
        eventBus.consumer(AddressRegister.MQTT, this::publish);

        //Define MQTT-EndpointHandler
        mqttServer.endpointHandler(endpoint -> {
            JsonObject userJson = new JsonObject();
            userJson.put("username", endpoint.auth().userName());
            userJson.put("password", endpoint.auth().password());
            eventBus.send(AddressRegister.GET_USER_LOGIN, userJson, res -> {
                if(res.succeeded()) {
                    JsonObject resultJson = (JsonObject) res.result().body();
                    int result = resultJson.getInteger(CoreConstants.HTTP_CODE);
                    if(result == HttpCodes.OK) {
                        endpoint.accept(false);
                        handleSubscription(endpoint);
                    } else {
                        endpoint.reject(MqttConnectReturnCode.CONNECTION_REFUSED_BAD_USER_NAME_OR_PASSWORD);
                    }
                } else {
                    endpoint.reject(MqttConnectReturnCode.CONNECTION_REFUSED_SERVER_UNAVAILABLE);
                }
            });
        });

        //Starting MQTT-Server
        mqttServer.listen(ar -> {
            if(ar.succeeded()) {
                startFuture.complete();
            } else {
                startFuture.fail(ar.cause());
            }
        });
    }

    private void handleSubscription(MqttEndpoint endpoint) {
        endpoint.subscribeHandler(subscribe -> {
            List<MqttQoS> grantedQosLevels = new ArrayList<>();
            for(MqttTopicSubscription s: subscribe.topicSubscriptions()) {
                grantedQosLevels.add(s.qualityOfService());
                endpoints.put(s.topicName(), endpoint);
            }
            endpoint.subscribeAcknowledge(subscribe.messageId(), grantedQosLevels);
        });
    }

    private void publish(Message<JsonObject> message) {
        JsonObject messageJson = message.body();
        String method = messageJson.getString("method");
        String component = messageJson.getString("component");
        String username = messageJson.getString("username");
        String key;
        if(component.equals("user") || component.equals("contact")) {
            key = Integer.toString(messageJson.getInteger("id"));
        } else {
            key = messageJson.getString("key");
        }

        String topic = method + "/" + username + "/" + component;
        Iterator<String> endpointIterator = endpoints.keySet().iterator();
        while(endpointIterator.hasNext()) {
            String endpointTopic = endpointIterator.next();
            if(endpointTopic.equals(topic)) {
                MqttEndpoint curEndpoint = endpoints.get(endpointTopic);
                if(username.equals(curEndpoint.auth().userName())) {
                    curEndpoint.publish(topic, Buffer.buffer(key), MqttQoS.AT_MOST_ONCE, false, false);
                }
            }
        }

    }

}
