package de.ingence.studyplanner.mqtt;

import de.ingence.studyplanner.mqtt.setup.ConfigSetupMqtt;
import io.vertx.core.Vertx;


public class Main {

    public static void main(String[] args) {
        try {
            if(ConfigSetupMqtt.setup()) {
                System.exit(1);
            }
            //Creating VERT.X Instance
            Vertx vertx = Vertx.vertx();
            //Deploy verticle
            vertx.deployVerticle(new MqttServer());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
