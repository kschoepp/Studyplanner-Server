package de.ingence.studyplanner.mqtt.constants;

import de.ingence.studyplanner.core.constants.ConsoleStyles;



public class Errors extends ConsoleStyles {

    //FileSetup errors
    public static final String FILESETUP_PROPERTIESFILE_CREATION_FILED = "Unable to create <mqtt.properties> file!";

}