import de.ingence.studyplanner.mqtt.MqttServer;
import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.mqtt.MqttClient;
import io.vertx.mqtt.MqttClientOptions;

/**
 * (C) Ingence
 * www.ingence.de
 * Created by Cedric Hopf on 06.03.2018
 **/
public class MqttTest {

    public static void main(String[] args) {
        try {
            Vertx vertx = Vertx.vertx();
            MqttClientOptions options = new MqttClientOptions();
            options.setUsername("chopf");
            options.setPassword("12345");
//            vertx.deployVerticle(new UserClient(), rex -> {
//                if(rex.succeeded()) {
//                    try {
//                        vertx.deployVerticle(new MqttServer(), res -> {
//                            if (res.succeeded()) {
//                                System.out.println("Server: MQTT Server started!");
//                                MqttClient client = MqttClient.create(vertx, options);
//                                client.connect(1883, "localhost", s -> {
//                                    if (s.succeeded()) {
//                                        System.out.println("Client: successfully connected");
//                                        client.publishHandler(x -> {
//                                            System.out.println("New message on topic " + x.topicName());
//                                            System.out.println("Message: " + x.payload().toString());
//                                        }).subscribe("post/chopf/todo", 0, as -> {
//                                        });
//                                        vertx.setPeriodic(200, h -> {
//                                            vertx.eventBus().send("mqtt", new JsonObject().put("username", "chopf").put("component", "todo").put("method", "post").put("key", "hola!"));
//                                        });
//                                    } else {
//                                        System.out.println(s.cause().toString());
//                                    }
//                                });
//                            } else {
//                                System.out.println(res.cause().toString());
//                            }
//                        });
//                    } catch (Exception x) {
//                        System.out.println(x.getMessage());
//                    }
//                } else {
//                    System.out.println(rex.cause().toString());
//                }
//            });

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
