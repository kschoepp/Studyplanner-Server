import io.vertx.core.Vertx;
import io.vertx.core.json.JsonObject;
import io.vertx.mqtt.MqttClient;
import io.vertx.mqtt.MqttClientOptions;

/**
 * (C) Ingence
 * www.ingence.de
 * Created by Cedric Hopf on 07.03.2018
 **/
public class SubscribeTest {

    public static void main(String[] args) {
        try {
            Vertx vertx = Vertx.vertx();
            MqttClientOptions options = new MqttClientOptions();
            options.setUsername("chopf");
            options.setPassword("827ccb0eea8a706c4c34a16891f84e7b");
            MqttClient client = MqttClient.create(vertx, options);
            client.connect(1883, "localhost", s -> {
                if (s.succeeded()) {
                    System.out.println("Client: successfully connected");
                    client.publishHandler(x -> {
                        System.out.println("New message on topic " + x.topicName());
                        System.out.println("Message: " + x.payload().toString());
                    }).subscribe("post/chopf/todo", 0, as -> {
                    });
                } else {
                    System.out.println(s.cause().toString());
                }
            });
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
