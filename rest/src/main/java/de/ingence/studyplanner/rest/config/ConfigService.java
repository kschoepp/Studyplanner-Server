package de.ingence.studyplanner.rest.config;

import de.ingence.studyplanner.rest.constants.RestConstants;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.Properties;


public class ConfigService {

    public static String getConfig(String param) throws Exception {
        Properties p = new Properties();
        BufferedInputStream b = new BufferedInputStream(new FileInputStream(RestConstants.CONFIG_DIR_PATH + "/" + RestConstants.CONFIG_FILE_REST));
        p.load(b);
        b.close();
        return p.getProperty(param);
    }

}
