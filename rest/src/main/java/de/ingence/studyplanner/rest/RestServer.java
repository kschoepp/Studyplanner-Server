package de.ingence.studyplanner.rest;

import de.ingence.studyplanner.core.constants.AddressRegister;
import de.ingence.studyplanner.core.constants.CoreConstants;
import de.ingence.studyplanner.core.constants.HttpCodes;
import de.ingence.studyplanner.rest.config.ConfigService;
import de.ingence.studyplanner.rest.constants.RestConstants;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.MultiMap;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.http.HttpMethod;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.handler.BodyHandler;
import io.vertx.ext.web.handler.CorsHandler;

import java.util.HashMap;
import java.util.Iterator;


public class RestServer extends AbstractVerticle {

    //Global variables
    private int port;
    //|---VERT.X Objects
    private EventBus eventBus;
    HashMap<String, String> activeUsers = new HashMap<>();

    /**
     * Default constructor to initialize REST-Server instance
     */
    public RestServer() throws Exception {
        initPort();
    }

    /**
     * Help function to read the HTTP-Port from configuration file
     * @throws Exception
     */
    private void initPort() throws Exception {
        String port = ConfigService.getConfig("port");
        if (port == null) {
            this.port = RestConstants.HTTP_PORT;
        } else {
            this.port = Integer.parseInt(port);
        }
    }

    /**
     * Start function executed by VERT.X Instance on verticle deployment
     * @param startFuture
     * @throws Exception
     */
    @Override
    public void start(Future<Void> startFuture) throws Exception {
        //=== First step ===
        //Creating router
        Router router = Router.router(vertx);
        //Getting EventBus from VERT.X instance
        this.eventBus = vertx.eventBus();

        //=======================
        //=== Define REST-API ===
        //=======================
        router.route().handler(CorsHandler.create("*")
                .allowedMethod(HttpMethod.GET)
                .allowedMethod(HttpMethod.POST)
                .allowedMethod(HttpMethod.PATCH)
                .allowedMethod(HttpMethod.DELETE)
                .allowedHeader("Access-Control-Allow-Origin")
                .allowedHeader("Content-Type"));
        //|---Define allowed body handling
        router.route("/api/user*").handler(BodyHandler.create());
        router.route("/api/user/register*").handler(BodyHandler.create());
        router.route("/api/user/password*").handler(BodyHandler.create());
        router.route("/api/todo*").handler(BodyHandler.create());
        router.route("/api/todo/done*").handler(BodyHandler.create());
        router.route("/api/course*").handler(BodyHandler.create());
        router.route("/api/exam*").handler(BodyHandler.create());
        router.route("/api/calendar*").handler(BodyHandler.create());
        router.route("/api/contact*").handler(BodyHandler.create());
        router.route("/api/contact/confirm*").handler(BodyHandler.create());
        //|---GET-Methods
        router.get("/api/user/login").handler(this::validateLogin);
        router.get("/api/user/details").handler(this::getUserDetails);
        router.get("/api/user/register/username").handler(this::checkUsername);
        router.get("/api/user/by/id").handler(this::getUserByID);
        router.get("/api/user/foreigndetails").handler(this::getForeignUserDetails);
        router.get("/api/todo/by/key").handler(this::getTodoByKey);
        router.get("/api/todo/by/user").handler(this::getTodosByUser);
        router.get("/api/calendar/by/key").handler(this::getCalendarEventByKey);
        router.get("/api/calendar/by/user").handler(this::getCalendarEventsByUser);
        router.get("/api/calendar/by/date").handler(this::getCalendarEventsByDate);
        router.get("/api/contact/by/user").handler(this::getContactsByUser);
        router.get("/api/exam/by/user").handler(this::getExamsByUser);
        router.get("/api/exam/by/key").handler(this::getExamByKey);
        router.get("/api/course/by/user").handler(this::getCoursesByUser);
        router.get("/api/course/by/key").handler(this::getCourseByKey);
        //|---POST-Methods
        router.post("/api/user/register").handler(this::registerUser);
        router.post("/api/todo").handler(this::addTodo);
        router.post("/api/course").handler(this::addCourse);
        router.post("/api/exam").handler(this::addExam);
        router.post("/api/calendar").handler(this::addCalendarEvent);
        router.post("/api/contact").handler(this::addContact);
        //|---PATCH-Methods
        router.patch("/api/user").handler(this::patchUser);
        router.patch("/api/user/password").handler(this::patchUserPassword);
        router.patch("/api/user/activate").handler(this::activateUser);
        router.patch("/api/todo").handler(this::patchTodo);
        router.patch("/api/todo/done").handler(this::patchTodoDone);
        router.patch("/api/course").handler(this::patchCourse);
        router.patch("/api/exam").handler(this::patchExam);
        router.patch("/api/calendar").handler(this::patchCalendarEvent);
        router.patch("/api/contact/confirm").handler(this::confirmContact);
        //|---DELETE-Methods
        router.delete("/api/user").handler(this::deleteUser);
        router.delete("/api/todo").handler(this::deleteTodo);
        router.delete("/api/course").handler(this::deleteCourse);
        router.delete("/api/exam").handler(this::deleteExam);
        router.delete("/api/calendar").handler(this::deleteCalendarEvent);
        router.delete("/api/contact").handler(this::deleteContact);


        eventBus.send(AddressRegister.GET_ALL_USER, new JsonObject(), res -> {
            JsonArray array = (JsonArray) res.result().body();
            for( int i = 0; i< array.size(); i++) {
                activeUsers.put(array.getJsonObject(i).getString("username"),array.getJsonObject(i).getString("password"));
            }
        });
        //=== Last step ===
        //Starting HTTP-Main
        HttpServer httpServer = vertx.createHttpServer();
        httpServer.requestHandler(router::accept).listen(port,
                result -> {
                    if (result.succeeded()) {
                        startFuture.complete();
                    } else {
                        startFuture.fail(result.cause());
                    }
                }
        );
    }

    //===============================
    //=== REST-Functions redirect ===
    //===============================
    //|---GET-Methods

    private void validateLogin(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("username", username);
        jsonObject.put("password", password);
        eventBus.send(AddressRegister.GET_USER_LOGIN, jsonObject, res -> {
            if(res.succeeded()) {
                JsonObject result = (JsonObject) res.result().body();
                rc.response()
                        .setStatusCode(result.getInteger(CoreConstants.HTTP_CODE))
                        .putHeader("content-type", "application/json; charset=utf-8")
                        .putHeader("Access-Control-Allow-Origin", "*")
                        .end(result.encodePrettily());
            } else {
                internalServerError(rc);
            }
        });
    }

    private void getUserDetails(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("username", username);
        jsonObject.put("password", password);
        if(isAuthorized(username,password)) {
            sendToEventBus(AddressRegister.GET_USER_DETAILS, jsonObject, rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }
    }

    private void getForeignUserDetails(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        String user_two = params.get("user_two");
        if(isAuthorized(username,password)) {
            sendToEventBus(AddressRegister.GET_USER_DETAILS, new JsonObject().put("username", user_two), rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }
    }


    private void checkUsername(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("username", username);
        sendToEventBus(AddressRegister.GET_USER_REGISTER_USERNAME, jsonObject, rc);
    }

    private void getUserByID(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        JsonObject jsonObject = new JsonObject();
        jsonObject.put("username", username);
        jsonObject.put("password", password);
        jsonObject.put("id", Integer.parseInt(params.get("id")));
        if(isAuthorized(username,password)) {
            sendToEventBus(AddressRegister.GET_USER_BY_ID, jsonObject, rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }
    }

    private void getContactsByUser(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        if(isAuthorized(username,password)) {
            eventBus.send(AddressRegister.GET_CONTACT_BY_USER, new JsonObject().put("username", username), ar -> {
                if(ar.succeeded()) {
                    JsonArray response = (JsonArray) ar.result().body();
                    rc.response()
                            .setStatusCode(response.getJsonObject(0).getInteger(CoreConstants.HTTP_CODE))
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .putHeader("Access-Control-Allow-Origin", "*")
                            .end(response.encodePrettily());
                } else {
                    internalServerError(rc);
                }
            });
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void getTodoByKey(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        String key = params.get("key");
        if(isAuthorized(username,password)) {
            sendToEventBus(AddressRegister.GET_TODO_BY_KEY, new JsonObject().put("key", key), rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void getTodosByUser(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        if(isAuthorized(username,password)) {
            eventBus.send(AddressRegister.GET_TODO_BY_USER, new JsonObject().put("username", username), ar -> {
                if(ar.succeeded()) {
                    JsonArray response = (JsonArray) ar.result().body();
                    rc.response()
                            .setStatusCode(response.getJsonObject(0).getInteger(CoreConstants.HTTP_CODE))
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .putHeader("Access-Control-Allow-Origin", "*")
                            .end(response.encodePrettily());
                } else {
                    internalServerError(rc);
                }
            });
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void getCalendarEventByKey(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        String key = params.get("key");
        if(isAuthorized(username,password)) {
            sendToEventBus(AddressRegister.GET_CALENDAR_BY_KEY, new JsonObject().put("key", key), rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void getCalendarEventsByUser(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        if(isAuthorized(username,password)) {
            eventBus.send(AddressRegister.GET_CALENDAR_BY_USER, new JsonObject().put("username", username), ar -> {
                if(ar.succeeded()) {
                    JsonArray response = (JsonArray) ar.result().body();
                    rc.response()
                            .setStatusCode(response.getJsonObject(0).getInteger(CoreConstants.HTTP_CODE))
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .putHeader("Access-Control-Allow-Origin", "*")
                            .end(response.encodePrettily());
                } else {
                    internalServerError(rc);
                }
            });
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void getCalendarEventsByDate(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        int owner = Integer.parseInt(params.get("owner"));
        long date = Long.parseLong(params.get("date"));
        if(isAuthorized(username,password)) {
            eventBus.send(AddressRegister.GET_CALENDAR_BY_DATE, new JsonObject().put("owner", owner).put("date", date), ar -> {
                if(ar.succeeded()) {
                    JsonArray response = (JsonArray) ar.result().body();
                    rc.response()
                            .setStatusCode(response.getJsonObject(0).getInteger(CoreConstants.HTTP_CODE))
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .putHeader("Access-Control-Allow-Origin", "*")
                            .end(response.encodePrettily());
                } else {
                    internalServerError(rc);
                }
            });
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void getExamsByUser(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        if(isAuthorized(username,password)) {
            eventBus.send(AddressRegister.GET_EXAM_BY_USER, new JsonObject().put("username", username), ar -> {
                if(ar.succeeded()) {
                    JsonArray response = (JsonArray) ar.result().body();
                    rc.response()
                            .setStatusCode(response.getJsonObject(0).getInteger(CoreConstants.HTTP_CODE))
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .putHeader("Access-Control-Allow-Origin", "*")
                            .end(response.encodePrettily());
                } else {
                    internalServerError(rc);
                }
            });
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void getExamByKey(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        String key = params.get("key");
        if(isAuthorized(username,password)) {
            sendToEventBus(AddressRegister.GET_EXAM_BY_KEY, new JsonObject().put("key", key), rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void getCoursesByUser(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        if(isAuthorized(username,password)) {
            eventBus.send(AddressRegister.GET_COURSE_BY_USER, new JsonObject().put("username", username), ar -> {
                if(ar.succeeded()) {
                    JsonArray response = (JsonArray) ar.result().body();
                    rc.response()
                            .setStatusCode(response.getJsonObject(0).getInteger(CoreConstants.HTTP_CODE))
                            .putHeader("content-type", "application/json; charset=utf-8")
                            .putHeader("Access-Control-Allow-Origin", "*")
                            .end(response.encodePrettily());
                } else {
                    internalServerError(rc);
                }
            });
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void getCourseByKey(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        String key = params.get("key");
        if(isAuthorized(username,password)) {
            sendToEventBus(AddressRegister.GET_COURSE_BY_KEY, new JsonObject().put("key", key), rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    //|---POST-Methods

    private void registerUser(RoutingContext rc) {
        JsonObject jsonObject = rc.getBodyAsJson();
        sendToEventBus(AddressRegister.POST_USER_REGISTER, jsonObject, rc);
    }

    private void addTodo(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        if(isAuthorized(username,password)) {
            JsonObject todoJson = rc.getBodyAsJson();
            todoJson.put("username", username);
            sendToEventBus(AddressRegister.POST_TODO, todoJson, rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void addCourse(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        if(isAuthorized(username,password)) {
            JsonObject courseJson = rc.getBodyAsJson();
            courseJson.put("username", username);
            sendToEventBus(AddressRegister.POST_COURSE, courseJson, rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void addExam(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        if(isAuthorized(username,password)) {
            JsonObject examJson = rc.getBodyAsJson();
            examJson.put("username", username);
            sendToEventBus(AddressRegister.POST_EXAM, examJson, rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void addCalendarEvent(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        if(isAuthorized(username,password)) {
            JsonObject calendarEventJson = rc.getBodyAsJson();
            calendarEventJson.put("username", username);
            sendToEventBus(AddressRegister.POST_CALENDAR, calendarEventJson, rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void addContact(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        if(isAuthorized(username,password)) {
            JsonObject contactJson = rc.getBodyAsJson();
            contactJson.put("username", username);
            sendToEventBus(AddressRegister.POST_CONTACT, contactJson, rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    //|---PATCH-Methods

    private void patchUser(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        if(isAuthorized(username,password)) {
            JsonObject userJson = rc.getBodyAsJson();
            userJson.put("username", username);
            sendToEventBus(AddressRegister.PATCH_USER, userJson, rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void patchUserPassword(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        if(isAuthorized(username,password)) {
            JsonObject userJson = rc.getBodyAsJson();
            userJson.put("username", username);
            sendToEventBus(AddressRegister.PATCH_USER_PASSWORD, userJson, rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void patchTodo(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        if(isAuthorized(username,password)) {
            JsonObject todoJson = rc.getBodyAsJson();
            todoJson.put("username", username);
            sendToEventBus(AddressRegister.PATCH_TODO, todoJson, rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void patchTodoDone(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        if(isAuthorized(username,password)) {
            JsonObject todoJson = new JsonObject();
            todoJson.put("username", username);
            todoJson.put("key", params.get("key"));
            sendToEventBus(AddressRegister.PATCH_TODO_DONE, todoJson, rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void patchCourse(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        if(isAuthorized(username,password)) {
            JsonObject courseJson = rc.getBodyAsJson();
            courseJson.put("username", username);
            sendToEventBus(AddressRegister.PATCH_COURSE, courseJson, rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void patchExam(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        if(isAuthorized(username,password)) {
            JsonObject examJson = rc.getBodyAsJson();
            examJson.put("username", username);
            sendToEventBus(AddressRegister.PATCH_EXAM, examJson, rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void patchCalendarEvent(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        if(isAuthorized(username,password)) {
            JsonObject calendarEventJson = rc.getBodyAsJson();
            calendarEventJson.put("username", username);
            sendToEventBus(AddressRegister.PATCH_CALENDAR, calendarEventJson, rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void confirmContact(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        String id = params.get("id");
        if(isAuthorized(username,password)) {
            JsonObject contactJson = new JsonObject(); /*= rc.getBodyAsJson();*/
            contactJson.put("id", id);
            sendToEventBus(AddressRegister.PATCH_CONTACT_CONFIRM, contactJson, rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void activateUser(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String activationKey = params.get("activation_key");
        eventBus.send(AddressRegister.PATCH_USER_ACTIVE, new JsonObject().put("activation_key", activationKey), res -> {
            if(res.succeeded()) {
                JsonObject result = (JsonObject) res.result().body();
                if(result.getInteger(CoreConstants.HTTP_CODE) == HttpCodes.OK) {
                    eventBus.send(AddressRegister.GET_USER_BY_ACT_KEY, new JsonObject().put("activation_key", activationKey), ar -> {
                        if(ar.succeeded()) {
                            JsonObject answer = (JsonObject) ar.result().body();
                            this.activeUsers.put(answer.getString("username"), answer.getString("password"));
                            sendResponse(rc, result);
                        } else {
                            internalServerError(rc);
                        }
                    });
                }
            } else {
                internalServerError(rc);
            }
        });
    }

    //|---DELETE-Methods

    private void deleteUser(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        int id = Integer.parseInt(params.get("id"));
        if(isAuthorized(username,password)) {
            sendToEventBus(AddressRegister.DELETE_USER, new JsonObject().put("id", id).put("username", username), rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void deleteTodo(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        String key = params.get("key");
        if(isAuthorized(username,password)) {
            sendToEventBus(AddressRegister.DELETE_TODO, new JsonObject().put("key", key).put("username", username), rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }

    }

    private void deleteCourse(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        String key = params.get("key");
        if(isAuthorized(username,password)) {
            sendToEventBus(AddressRegister.DELETE_COURSE, new JsonObject().put("key", key).put("username", username), rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void deleteExam(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        String key = params.get("key");
        if(isAuthorized(username,password)) {
            sendToEventBus(AddressRegister.DELETE_EXAM, new JsonObject().put("key", key).put("username", username), rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void deleteCalendarEvent(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        String key = params.get("key");
        if(isAuthorized(username,password)) {
            sendToEventBus(AddressRegister.DELETE_CALENDAR, new JsonObject().put("key", key).put("username", username), rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }


    }

    private void deleteContact(RoutingContext rc) {
        MultiMap params = rc.request().params();
        String username = params.get("username");
        String password = params.get("password");
        int id = Integer.parseInt(params.get("id"));
        if(isAuthorized(username,password)) {
            sendToEventBus(AddressRegister.DELETE_CONTACT, new JsonObject().put("id", id).put("username", username), rc);
        } else {
            rc.response()
                    .setStatusCode(HttpCodes.UNAUTHORIZED)
                    .putHeader("content-type", "application/json; charset=utf-8")
                    .putHeader("Access-Control-Allow-Origin", "*")
                    .end(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.UNAUTHORIZED).encodePrettily());
        }
    }

    //Help functions
    private void sendToEventBus(String address, JsonObject jsonObject, RoutingContext rc) {
        eventBus.send(address, jsonObject, res -> {
            if(res.succeeded()) {
                JsonObject result = (JsonObject) res.result().body();
                sendResponse(rc, result);
            } else {
                internalServerError(rc);
            }
        });
    }

    private void sendResponse(RoutingContext rc, JsonObject response) {
        rc.response()
                .setStatusCode(response.getInteger(CoreConstants.HTTP_CODE))
                .putHeader("content-type", "application/json; charset=utf-8")
                .putHeader("Access-Control-Allow-Origin", "*")
                .end(response.encodePrettily());
    }

    private void internalServerError(RoutingContext rc) {
        JsonObject result = new JsonObject();
        result.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
        rc.response()
                .setStatusCode(HttpCodes.INTERNAL_SERVER_ERROR)
                .putHeader("content-type", "application/json; charset=utf-8")
                .putHeader("Access-Control-Allow-Origin", "*")
                .end(result.encodePrettily());
    }

    private boolean isAuthorized(String username, String password){
        Iterator<String> iterator = activeUsers.keySet().iterator();
        while(iterator.hasNext()) {
            String user_name = iterator.next();
            if(user_name.equals(username)){
                if(password.equals(activeUsers.get(user_name))){
                    return true;
                }
            }
        }
        return false;
    }

}
