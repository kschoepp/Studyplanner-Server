package de.ingence.studyplanner.rest;

import de.ingence.studyplanner.rest.setup.ConfigSetupRest;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;


public class Main {

    /**
     * Main function to deploy REST-RestServer using VERT.X
     * @param args
     */
    public static void main(String[] args) {
        try {
            if(ConfigSetupRest.setup()) {
                System.exit(1);
            }
            //Creating VertxOptions
            VertxOptions options = new VertxOptions();
            options.setBlockedThreadCheckInterval(1000 * 60 * 60);
            //Creating VERT.X Instance
            Vertx vertx = Vertx.vertx(options);
            //Deploy verticle
            vertx.deployVerticle(new RestServer());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
