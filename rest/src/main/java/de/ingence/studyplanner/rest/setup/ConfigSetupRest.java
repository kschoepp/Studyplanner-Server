package de.ingence.studyplanner.rest.setup;

import de.ingence.studyplanner.rest.constants.Errors;
import de.ingence.studyplanner.rest.constants.Messages;
import de.ingence.studyplanner.rest.constants.RestConstants;
import de.ingence.studyplanner.rest.constants.SetupConstants;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;

public class ConfigSetupRest {

    public static boolean setup() throws Exception {
        //server.properties
        File propertiesFile = new File(RestConstants.CONFIG_DIR_PATH + "/" + RestConstants.CONFIG_FILE_REST);
        if(propertiesFile.exists()) {
            System.out.println(Messages.curDate() + Messages.INFORMATION + Messages.FILESETUP_PROPERTIESFILE_ALREADY_EXISTS);
            return false;
        } else {
            File dir = new File(RestConstants.CONFIG_DIR_PATH);
            if(!dir.exists()) {
                dir.mkdir();
            }
            System.out.println(Messages.curDate() + Messages.INFORMATION + Messages.FILESETUP_PROPERTIESFILE_START_SETUP);
            if(propertiesFile.createNewFile()) {
                System.out.println(Messages.curDate() + Messages.INFORMATION + Messages.FILESETUP_PROPERTIESFILE_CREATED);
                FileWriter fileWriter = new FileWriter(RestConstants.CONFIG_DIR_PATH + "/" + RestConstants.CONFIG_FILE_REST);
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                bufferedWriter.write(SetupConstants.PROPERTIES_VALUES);
                bufferedWriter.close();
                System.out.println(Messages.curDate() + Messages.INFORMATION + Messages.FILESETUP_PROPERTIESFILE_INIT);
                return true;
            } else {
                throw new Exception(Errors.curDate() + Errors.ERROR + Errors.FILESETUP_PROPERTIESFILE_CREATION_FILED);
            }
        }

    }
}
