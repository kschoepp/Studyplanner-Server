package de.ingence.studyplanner.rest.constants;


public class RestConstants {

    //Configuration default values
    //|---VERT.X default values
    public static final int HTTP_PORT = 80;
    //File paths
    public static final String CONFIG_DIR_PATH = "./config";
    public static final String CONFIG_FILE_REST = "rest.properties";


}
