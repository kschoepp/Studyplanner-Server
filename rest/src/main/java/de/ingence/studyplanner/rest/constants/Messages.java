package de.ingence.studyplanner.rest.constants;

import de.ingence.studyplanner.core.constants.ConsoleStyles;



public class Messages extends ConsoleStyles {

    //FileSetup messages
    public static final String FILESETUP_PROPERTIESFILE_ALREADY_EXISTS = "Skipping creating <rest.properties> file. File already exists...";
    public static final String FILESETUP_PROPERTIESFILE_START_SETUP = "Creating <rest.properties> file...";
    public static final String FILESETUP_PROPERTIESFILE_CREATED = "File <rest.properties> created";
    public static final String FILESETUP_PROPERTIESFILE_INIT = "File <rest.properties> initializied";


}
