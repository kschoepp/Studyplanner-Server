import de.ingence.studyplanner.core.Core;
import de.ingence.studyplanner.core.setup.ConfigSetupCore;
import de.ingence.studyplanner.mqtt.MqttServer;
import de.ingence.studyplanner.mqtt.setup.ConfigSetupMqtt;
import de.ingence.studyplanner.rest.RestServer;
import de.ingence.studyplanner.rest.setup.ConfigSetupRest;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;


public class Server {

    public static void main(String[] args) {
        try {
            boolean coreSetup = ConfigSetupCore.setup();
            boolean mqttSetup = ConfigSetupMqtt.setup();
            boolean restSetup = ConfigSetupRest.setup();
            if(coreSetup || mqttSetup || restSetup) {
                System.exit(1);
            }
            //Creating VertxOptions
            VertxOptions options = new VertxOptions();
            options.setBlockedThreadCheckInterval(1000 * 60 * 60);
            //Creating VERT.X Instance
            Vertx vertx = Vertx.vertx(options);
            //Deploy verticle
            //|---Core
            vertx.deployVerticle(new Core(), ar -> {
                if(ar.succeeded()) {
                    System.out.println("Core started!");
                }
            });
            //|---REST-Server
            vertx.deployVerticle(new RestServer(), ar -> {
                if(ar.succeeded()) {
                    System.out.println("REST-Server started!");
                }
            });
            //|---MQTT-Server
            vertx.deployVerticle(new MqttServer(), ar -> {
                if(ar.succeeded()) {
                    System.out.println("MQTT-Server started!");
                }
            });
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
