CREATE TABLE IF NOT EXISTS "user" (
  id SERIAL PRIMARY KEY NOT NULL,
  username TEXT NOT NULL,
  firstname TEXT NOT NULL,
  lastname TEXT NOT NULL,
  email TEXT NOT NULL,
  password TEXT NOT NULL,
  birthday TEXT NULL,
  location TEXT NULL,
  picture TEXT NULL,
  active BOOLEAN NOT NULL,
  activation_key TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS "todo" (
  key TEXT PRIMARY KEY NOT NULL,
  task TEXT NOT NULL,
  owner INT REFERENCES "user"(id),
  priority TEXT NOT NULL,
  due TEXT NULL,
  done BOOLEAN NOT NULL,
  created TEXT NOT NULL,
  participants TEXT NULL
);

CREATE TABLE IF NOT EXISTS "calendar" (
  key TEXT PRIMARY KEY NOT NULL,
  topic TEXT NOT NULL,
  owner INT REFERENCES "user"(id) NOT NULL,
  date_start BIGINT NOT NULL,
  date_end BIGINT NOT NULL,
  location TEXT NULL,
  participants TEXT NULL,
  created TEXT NOT NULL
);

CREATE TABLE IF NOT EXISTS "course" (
  key TEXT PRIMARY KEY NOT NULL,
  title TEXT NOT NULL,
  owner INT REFERENCES "user"(id) NOT NULL,
  day INT NOT NULL,
  date_start BIGINT NOT NULL,
  date_end BIGINT NOT NULL,
  location TEXT NULL
);

CREATE TABLE IF NOT EXISTS "exam" (
  key TEXT PRIMARY KEY NOT NULL,
  title TEXT NOT NULL,
  owner INT REFERENCES "user"(id) NOT NULL,
  date_start BIGINT NOT NULL,
  date_end BIGINT NOT NULL,
  location TEXT NULL
);

CREATE TABLE IF NOT EXISTS "contact" (
  id SERIAL PRIMARY KEY NOT NULL,
  creator INT REFERENCES  "user"(id) NOT NULL,
  user_one INT REFERENCES "user"(id) NOT NULL,
  user_two INT REFERENCES "user"(id) NOT NULL,
  confirmed BOOLEAN NOT NULL
)