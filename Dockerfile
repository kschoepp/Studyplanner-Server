FROM maven:alpine as build
WORKDIR /app
COPY / /app
RUN mvn install -e

FROM java:8-alpine
WORKDIR /app
COPY --from=build /app/aio/target/aio-1.0-jar-with-dependencies.jar /app/server-aio.jar
#COPY /aio/target/aio-1.0-jar-with-dependencies.jar /app/server-aio.jar
EXPOSE 80 1883
CMD ["java", "-jar", "/app/server-aio.jar"]
