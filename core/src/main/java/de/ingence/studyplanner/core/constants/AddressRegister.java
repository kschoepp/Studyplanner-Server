package de.ingence.studyplanner.core.constants;


public class AddressRegister {

    //EventBus addresses
    //|---GET
    public static final String GET_ALL_USER = "get/user";
    public static final String GET_USER_LOGIN = "get/user/login";
    public static final String GET_USER_DETAILS = "get/user/details";
    public static final String GET_USER_REGISTER_USERNAME = "get/user/register/username";
    public static final String GET_USER_BY_ID = "get/user/by/id";
    public static final String GET_USER_BY_ACT_KEY = "get/user/by/activationkey";
    public static final String GET_TODO_BY_KEY = "get/todo/by/key";
    public static final String GET_TODO_BY_USER = "get/todo/by/user";
    public static final String GET_CALENDAR_BY_KEY = "get/calendar/by/key";
    public static final String GET_CALENDAR_BY_USER = "get/calendar/by/user";
    public static final String GET_CALENDAR_BY_DATE = "get/calendar/by/date";
    public static final String GET_COURSE_BY_KEY = "get/course/by/key";
    public static final String GET_COURSE_BY_USER = "get/course/by/user";
    public static final String GET_EXAM_BY_KEY = "get/exam/by/key";
    public static final String GET_EXAM_BY_USER = "get/exam/by/user";
    public static final String GET_CONTACT_BY_ID = "get/contact/by/id";
    public static final String GET_CONTACT_BY_USER = "get/contact/by/user";
    //|---POST
    public static final String POST_USER_REGISTER = "post/user/register";
    public static final String POST_TODO = "post/todo";
    public static final String POST_COURSE = "post/course";
    public static final String POST_EXAM = "post/exam";
    public static final String POST_CALENDAR = "post/calendar";
    public static final String POST_CONTACT = "post/contact";
    //|---PATCH
    public static final String PATCH_USER = "patch/user";
    public static final String PATCH_USER_PASSWORD = "patch/user/password";
    public static final String PATCH_USER_ACTIVE = "patch/user/active";
    public static final String PATCH_TODO = "patch/todo";
    public static final String PATCH_TODO_DONE = "patch/todo/done";
    public static final String PATCH_COURSE = "patch/course";
    public static final String PATCH_EXAM = "patch/exam";
    public static final String PATCH_CALENDAR = "patch/calender";
    public static final String PATCH_CONTACT_CONFIRM = "patch/contact/confirm";
    //|---DELETE
    public static final String DELETE_USER = "delete/user";
    public static final String DELETE_TODO = "delete/todo";
    public static final String DELETE_COURSE = "delete/course";
    public static final String DELETE_EXAM = "delete/exam";
    public static final String DELETE_CALENDAR = "delete/calendar";
    public static final String DELETE_CONTACT = "delete/contact";
    //|---MQTT
    public static final String MQTT = "mqtt";

}
