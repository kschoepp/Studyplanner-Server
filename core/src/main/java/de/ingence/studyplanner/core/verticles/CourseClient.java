package de.ingence.studyplanner.core.verticles;

import de.ingence.studyplanner.core.config.ConfigService;
import de.ingence.studyplanner.core.constants.*;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.asyncsql.PostgreSQLClient;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;


public class CourseClient extends AbstractVerticle {

    //Global variables
    private Future<Void> future;
    private int method;
    //|---VERT.X Objects
    private Message<Object> message;
    private SQLClient courseClient;

    public CourseClient(int method, Message<Object> message) {
        this.method = method;
        this.message = message;
    }

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        this.future = startFuture;
        //Creating database client
        JsonObject clientConfig = ConfigService.getDatabaseConfig();
        this.courseClient = PostgreSQLClient.createShared(vertx, clientConfig);

        switch (method) {
            //|---GET
            case MethodRegister.GET_COURSE_BY_KEY:
                this.getCourseByKey();
                break;
            case MethodRegister.GET_COURSE_BY_USER:
                this.getCourseByUser();
                break;
            //|---POST
            case MethodRegister.POST_COURSE:
                this.createCourse();
                break;
            //|---PATCH
            case MethodRegister.PATCH_COURSE:
                this.updateCourse();
                break;
            //|---DELETE
            case MethodRegister.DELETE_COURSE:
                this.deleteCourse();
                break;
            default:
                break;
        }
    }

    /**
     * Method to create a course
     * ---------------------------------------------
     * EventBus-Address: POST_COURSE
     * ---------------------------------------------
     * Param:       JSON-Object with the following values
     *              "username":     String username
     *              "password":     String password
     *              "key":          String key
     *              "title":        String title
     *              "owner":        int id
     *              "day":          int day
     *              "time_start":   String time_start
     *              "time_end":     String time_end
     *              "location":     String location
     * ---------------------------------------------
     * Reply to EventBus:
     *              JSON-Object with the following values
     *              "httpCode":     int HTTP-Code
     *              "serverCode":   int Server-Code
     */
    private void createCourse() {
        JsonObject output = new JsonObject();
        courseClient.getConnection(con -> {
            if(con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String key = messageJson.getString("key");
                String title = messageJson.getString("title");
                int owner = messageJson.getInteger("owner");
                int day = messageJson.getInteger("day");
                long date_start = messageJson.getLong("date_start");
                long date_end = messageJson.getLong("date_end");
                if( title == null ){
                    output.put(CoreConstants.HTTP_CODE, HttpCodes.BAD_REQUEST);
                    output.put(CoreConstants.SERVER_CODE, ServerCodes.COURSEDATA_MISSING);
                    message.reply(output);
                } else {
                    String location = messageJson.getString("location");
                    String query = "INSERT INTO " + DatabaseConstants.TABLE_COURSE + " (" +
                            DatabaseConstants.COURSE_COLUMN_KEY + ", " +
                            DatabaseConstants.COURSE_COLUMN_TITLE + ", " +
                            DatabaseConstants.COURSE_COLUMN_OWNER + ", " +
                            DatabaseConstants.COURSE_COLUMN_DAY + ", " +
                            DatabaseConstants.COURSE_COLUMN_DATE_START + ", " +
                            DatabaseConstants.COURSE_COLUMN_DATE_END;
                    if(location != null) { query += ", " + DatabaseConstants.COURSE_COLUMN_LOCATION;}
                    query += " ) VALUES (" +
                            " '" + key + "', '" + title + "', " + owner + ", " + day + ", " + date_start + ", " + date_end;
                    if(location != null) {query += ", '" + location + "'";}
                    query += " )";
                    connection.update(query, res -> {
                        if(res.succeeded()){
                            output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                            message.reply(output);
                            vertx.eventBus().send(AddressRegister.MQTT,
                                    new JsonObject().put("username",
                                            messageJson.getString("username")).put("component", "course").put("method", "post").put("key", key ));
                        } else {
                            output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                            message.reply(output);
                        }
                    });
                }
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }


    /**
     * Method to delete a course
     * ---------------------------------------------
     * EventBus-Address: DELETE_COURSE
     * ---------------------------------------------
     * Param:       JSON-Object with the following values
     *              "username":     String username
     *              "password":     String password
     *              "key":          String key
     * ---------------------------------------------
     * Reply to EventBus:
     *              JSON-Object with the following values
     *              "httpCode":     int HTTP-Code
     *              "serverCode":   int Server-Code
     */
    private void deleteCourse() {
        JsonObject output = new JsonObject();
        courseClient.getConnection(con -> {
            if (con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String key = messageJson.getString("key");
                String query = "DELETE FROM " + DatabaseConstants.TABLE_COURSE + " WHERE " +
                        DatabaseConstants.COURSE_COLUMN_KEY + " = '" + key + "'";
                connection.update(query, res-> {
                    if(res.succeeded()){
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                        message.reply(output);
                        vertx.eventBus().send(AddressRegister.MQTT,
                                new JsonObject().put("username",
                                        messageJson.getString("username")).put("component", "course").put("method", "delete").put("key", key ));
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }


    /**
     * Method to update a course
     * ---------------------------------------------
     * EventBus-Address: PATCH_COURSE
     * ---------------------------------------------
     * Param:       JSON-Object with the following values
     *              "username":     String username
     *              "password":     String password
     *              "key":          String key
     *              "title":        String title
     *              "owner":        int id
     *              "day":          int day
     *              "time_start":   String time_start
     *              "time_end":     String time_end
     *              "location":     String location
     * ---------------------------------------------
     * Reply to EventBus:
     *              JSON-Object with the following values
     *              "httpCode":     int HTTP-Code
     *              "serverCode":   int Server-Code
     */
    private void updateCourse() {
        JsonObject output = new JsonObject();
        courseClient.getConnection(con -> {
            if (con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String key = messageJson.getString("key");
                String title = messageJson.getString("title");
                int owner = messageJson.getInteger("owner");
                int day = messageJson.getInteger("day");
                long date_start = messageJson.getLong("date_start");
                long date_end = messageJson.getLong("date_end");
                if(title == null ) {
                    output.put(CoreConstants.HTTP_CODE, HttpCodes.BAD_REQUEST);
                    output.put(CoreConstants.SERVER_CODE, ServerCodes.COURSEDATA_MISSING);
                    message.reply(output);
                }
                String location = messageJson.getString("location");
                String query = "UPDATE " + DatabaseConstants.TABLE_COURSE + " SET " +
                        DatabaseConstants.COURSE_COLUMN_TITLE + " = '" + title + "', " +
                        DatabaseConstants.COURSE_COLUMN_OWNER + " = " + owner + ", " +
                        DatabaseConstants.COURSE_COLUMN_DAY + " = " + day + ", " +
                        DatabaseConstants.COURSE_COLUMN_DATE_START + " = " + date_start + ", " +
                        DatabaseConstants.COURSE_COLUMN_DATE_END + " = " + date_end;
                if(location != null){query += ", " + DatabaseConstants.COURSE_COLUMN_LOCATION + " = '" + location + "'";}
                query += " WHERE " + DatabaseConstants.COURSE_COLUMN_KEY + " = '" + key + "'";
                connection.update(query, res-> {
                    if(res.succeeded()){
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                        message.reply(output);
                        vertx.eventBus().send(AddressRegister.MQTT,
                                new JsonObject().put("username",
                                        messageJson.getString("username")).put("component", "course").put("method", "patch").put("key", key ));
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

    private void getCourseByKey() {
        JsonObject output = new JsonObject();
        courseClient.getConnection(con -> {
            if (con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String key = messageJson.getString("key");
                String query = "SELECT * FROM " + DatabaseConstants.TABLE_COURSE + " WHERE " +
                        DatabaseConstants.COURSE_COLUMN_KEY + " = '" + key + "'";
                connection.query(query, res -> {
                    if (res.succeeded()) {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                        output.put("key", res.result().getRows().get(0).getString("key"));
                        output.put("title", res.result().getRows().get(0).getString("title"));
                        output.put("owner", res.result().getRows().get(0).getInteger("owner"));
                        output.put("day", res.result().getRows().get(0).getInteger("day"));
                        output.put("time_start", res.result().getRows().get(0).getString("time_start"));
                        output.put("time_end", res.result().getRows().get(0).getString("time_end"));
                        output.put("location", res.result().getRows().get(0).getString("location"));
                        message.reply(output);
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

    private void getCourseByUser() {
        JsonObject output = new JsonObject();
        JsonArray ja = new JsonArray();
        courseClient.getConnection(con -> {
            if (con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String username = messageJson.getString("username");
                String queryUsername = "SELECT id FROM " + DatabaseConstants.TABLE_USER + " WHERE " +
                        DatabaseConstants.USER_COLUMN_USERNAME + " = '" + username + "'";
                connection.query(queryUsername, resUsername -> {
                    if(resUsername.succeeded()) {
                        int owner = resUsername.result().getRows().get(0).getInteger("id");
                        String query = "SELECT * FROM " + DatabaseConstants.TABLE_COURSE + " WHERE " +
                                DatabaseConstants.COURSE_COLUMN_OWNER + " = " + owner + " ORDER BY " + DatabaseConstants.COURSE_COLUMN_DATE_START + " ASC";
                        connection.query(query, res -> {
                            if (res.succeeded()) {
                                output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                                ja.add(output);
                                for (int i = 0; i < res.result().getRows().size(); i++){
                                    JsonObject courseList = new JsonObject();
                                    courseList.put("key", res.result().getRows().get(i).getString("key"));
                                    courseList.put("title", res.result().getRows().get(i).getString("title"));
                                    courseList.put("owner", res.result().getRows().get(i).getInteger("owner"));
                                    courseList.put("day", res.result().getRows().get(i).getInteger("day"));
                                    courseList.put("date_start", res.result().getRows().get(i).getLong("date_start"));
                                    courseList.put("date_end", res.result().getRows().get(i).getLong("date_end"));
                                    courseList.put("location", res.result().getRows().get(i).getString("location"));
                                    ja.add(courseList);
                                }
                                message.reply(ja);
                            } else {
                                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                                message.reply(output);
                            }

                        });
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

}
