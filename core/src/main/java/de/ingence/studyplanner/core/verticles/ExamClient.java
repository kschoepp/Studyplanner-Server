package de.ingence.studyplanner.core.verticles;

import de.ingence.studyplanner.core.config.ConfigService;
import de.ingence.studyplanner.core.constants.*;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.asyncsql.PostgreSQLClient;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;


public class ExamClient extends AbstractVerticle {

    //Global variables
    private Future<Void> future;
    private int method;
    //|---VERT.X Objects
    private Message<Object> message;
    private SQLClient examClient;

    public ExamClient(int method, Message<Object> message) {
        this.method = method;
        this.message = message;
    }

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        this.future = startFuture;
        //Creating database client
        JsonObject clientConfig = ConfigService.getDatabaseConfig();
        this.examClient = PostgreSQLClient.createShared(vertx, clientConfig);

        switch (method) {
            //|---GET
            case MethodRegister.GET_EXAM_BY_USER:
                this.getExamByUser();
                break;
            case MethodRegister.GET_EXAM_BY_KEY:
                this.getExamByKey();
                break;
            //|---POST
            case MethodRegister.POST_EXAM:
                this.createExam();
                break;
            //|---PATCH
            case MethodRegister.PATCH_EXAM:
                this.updateExam();
                break;
            //|---DELETE
            case MethodRegister.DELETE_EXAM:
                this.deleteExam();
                break;
            default:
                break;
        }
    }

    private void createExam() {
        JsonObject output = new JsonObject();
        examClient.getConnection(con -> {
            if(con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String key = messageJson.getString("key");
                String title = messageJson.getString("title");
                int owner = messageJson.getInteger("owner");
                long date_start = messageJson.getLong("date_start");
                long date_end = messageJson.getLong("date_end");
                if( title == null ){
                    output.put(CoreConstants.HTTP_CODE, HttpCodes.BAD_REQUEST);
                    output.put(CoreConstants.SERVER_CODE, ServerCodes.EXAMDATA_MISSING);
                    message.reply(output);
                } else {
                    String location = messageJson.getString("location");
                    String query = "INSERT INTO " + DatabaseConstants.TABLE_EXAM + " (" +
                            DatabaseConstants.EXAM_COLUMN_KEY + ", " +
                            DatabaseConstants.EXAM_COLUMN_TITLE + ", " +
                            DatabaseConstants.EXAM_COLUMN_OWNER + ", " +
                            DatabaseConstants.EXAM_COLUMN_DATE_START + ", " +
                            DatabaseConstants.EXAM_COLUMN_DATE_END;
                    if(location != null || !location.isEmpty()) { query += ", " + DatabaseConstants.EXAM_COLUMN_LOCATION;}
                    query += " ) VALUES (" +
                            " '" + key + "', '" + title + "', " + owner + ", " + date_start + ", " + date_end;
                    if(location != null || !location.isEmpty()) {query += ", '" + location + "'";}
                    query += ")";
                    connection.update(query, res -> {
                        if(res.succeeded()){
                            output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                            message.reply(output);
                            vertx.eventBus().send(AddressRegister.MQTT,
                                    new JsonObject().put("username",
                                            messageJson.getString("username")).put("component", "exam").put("method", "post").put("key", key ));
                        } else {
                            output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                            message.reply(output);
                        }
                    });
                }
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

    private void deleteExam() {
        JsonObject output = new JsonObject();
        examClient.getConnection(con -> {
            if (con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String key = messageJson.getString("key");
                String query = "DELETE FROM " + DatabaseConstants.TABLE_EXAM + " WHERE " +
                        DatabaseConstants.EXAM_COLUMN_KEY + " = '" + key + "'";
                connection.update(query, res-> {
                    if(res.succeeded()){
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                        message.reply(output);
                        vertx.eventBus().send(AddressRegister.MQTT,
                                new JsonObject().put("username",
                                        messageJson.getString("username")).put("component", "exam").put("method", "delete").put("key", key ));
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

    private void updateExam() {
        JsonObject output = new JsonObject();
        examClient.getConnection(con -> {
            if (con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String key = messageJson.getString("key");
                String title = messageJson.getString("title");
                int owner = messageJson.getInteger("owner");
                long date_start = messageJson.getLong("date_start");
                long date_end = messageJson.getLong("date_end");
                if(title == null ) {
                    output.put(CoreConstants.HTTP_CODE, HttpCodes.BAD_REQUEST);
                    output.put(CoreConstants.SERVER_CODE, ServerCodes.EXAMDATA_MISSING);
                    message.reply(output);
                }
                String location = messageJson.getString("location");
                String query = "UPDATE " + DatabaseConstants.TABLE_EXAM+ " SET " +
                        DatabaseConstants.EXAM_COLUMN_TITLE + " = '" + title + "', " +
                        DatabaseConstants.EXAM_COLUMN_OWNER + " = " + owner + ", " +
                        DatabaseConstants.EXAM_COLUMN_DATE_START + " = " + date_start + ", " +
                        DatabaseConstants.EXAM_COLUMN_DATE_END + " = " + date_end;
                if(location != null){query += ", " + DatabaseConstants.EXAM_COLUMN_LOCATION + " = '" + location + "'";}
                query += " WHERE " + DatabaseConstants.EXAM_COLUMN_KEY + " = '" + key + "'";
                connection.update(query, res-> {
                    if(res.succeeded()){
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                        vertx.eventBus().send(AddressRegister.MQTT,
                                new JsonObject().put("username",
                                        messageJson.getString("username")).put("component", "exam").put("method", "patch").put("key", key ));
                        message.reply(output);
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

    private void getExamByKey() {
        JsonObject output = new JsonObject();
        examClient.getConnection(con -> {
            if (con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String key = messageJson.getString("key");
                String query = "SELECT * FROM " + DatabaseConstants.TABLE_EXAM + " WHERE " +
                        DatabaseConstants.EXAM_COLUMN_KEY + " = '" + key + "'";
                connection.query(query, res -> {
                    if (res.succeeded()) {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                        output.put("key", res.result().getRows().get(0).getString("key"));
                        output.put("title", res.result().getRows().get(0).getString("title"));
                        output.put("owner", res.result().getRows().get(0).getInteger("owner"));
                        output.put("date_start", res.result().getRows().get(0).getLong("date_start"));
                        output.put("date_end", res.result().getRows().get(0).getLong("date_end"));
                        output.put("location", res.result().getRows().get(0).getString("location"));
                        message.reply(output);
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

    private void getExamByUser() {
        JsonObject output = new JsonObject();
        JsonArray ja = new JsonArray();
        examClient.getConnection(con -> {
            if (con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String username = messageJson.getString("username");
                String queryUsername = "SELECT id FROM " + DatabaseConstants.TABLE_USER + " WHERE " +
                        DatabaseConstants.USER_COLUMN_USERNAME + " = '" + username + "'";
                connection.query(queryUsername, resUsername -> {
                    if(resUsername.succeeded()) {
                        int owner = resUsername.result().getRows().get(0).getInteger("id");
                        String query = "SELECT * FROM " + DatabaseConstants.TABLE_EXAM + " WHERE " +
                                DatabaseConstants.EXAM_COLUMN_OWNER + " = " + owner + "";
                        connection.query(query, res -> {
                            if (res.succeeded()) {
                                output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                                ja.add(output);
                                for (int i = 0; i < res.result().getRows().size(); i++){
                                    JsonObject examList = new JsonObject();
                                    examList.put("key", res.result().getRows().get(i).getString("key"));
                                    examList.put("title", res.result().getRows().get(i).getString("title"));
                                    examList.put("owner", res.result().getRows().get(i).getInteger("owner"));
                                    examList.put("date_start", res.result().getRows().get(i).getLong("date_start"));
                                    examList.put("date_end", res.result().getRows().get(i).getLong("date_end"));
                                    examList.put("location", res.result().getRows().get(i).getString("location"));
                                    ja.add(examList);
                                }
                                message.reply(ja);
                            } else {
                                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                                message.reply(output);
                            }

                        });
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

}
