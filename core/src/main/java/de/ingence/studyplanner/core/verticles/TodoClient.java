package de.ingence.studyplanner.core.verticles;

import de.ingence.studyplanner.core.config.ConfigService;
import de.ingence.studyplanner.core.constants.*;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.asyncsql.PostgreSQLClient;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;


public class TodoClient extends AbstractVerticle {

    //Global variables
    private Future<Void> future;
    private int method;
    //|---VERT.X Objects
    private Message<Object> message;
    private SQLClient todoClient;

    public TodoClient(int method, Message<Object> message) {
        this.method = method;
        this.message = message;
    }

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        this.future = startFuture;
        //Creating database client
        JsonObject clientConfig = ConfigService.getDatabaseConfig();
        this.todoClient = PostgreSQLClient.createShared(vertx, clientConfig);

        switch (method) {
            //|---GET
            case MethodRegister.GET_TODO_BY_USER:
                this.getTodoByUser();
                break;
            case MethodRegister.GET_TODO_BY_KEY:
                this.getTodoByKey();
                break;
            //|---POST
            case MethodRegister.POST_TODO:
                this.createTodo();
                break;
            //|---PATCH
            case MethodRegister.PATCH_TODO:
                this.updateTodo();
                break;
            case MethodRegister.PATCH_TODO_DONE:
                this.setTodoDone();
                break;
            //|---DELETE
            case MethodRegister.DELETE_TODO:
                this.deleteTodo();
                break;
            default:
                break;
        }
    }

    /**
     * Method to create a to-do item
     * ---------------------------------------------
     * EventBus-Address: POST_TODO
     * ---------------------------------------------
     * Param:       JSON-Object with the following values
     * "username":     String username
     * "password":     String password
     * "key":          String key
     * "task":         String task
     * "owner":        int id
     * "priority":     String priority
     * "due":          String due
     * "done":         boolean done
     * "created":      String created
     * "participants"  String participants
     * ---------------------------------------------
     * Reply to EventBus:
     * JSON-Object with the following values
     * "httpCode":     int HTTP-Code
     * "serverCode":   int Server-Code
     */
    private void createTodo() {
        JsonObject output = new JsonObject();
        todoClient.getConnection(con -> {
            if (con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String key = messageJson.getString("key");
                String task = messageJson.getString("task");
                int owner = messageJson.getInteger("owner");
                String priority = messageJson.getString("priority");
                String created = messageJson.getString("created");
                if (task == null || priority == null || key == null) {
                    output.put(CoreConstants.HTTP_CODE, HttpCodes.BAD_REQUEST);
                    output.put(CoreConstants.SERVER_CODE, ServerCodes.TODODATA_MISSING);
                    message.reply(output);
                }
                String due = messageJson.getString("due");
                String participants = messageJson.getString("participants");
                String query = "INSERT INTO " + DatabaseConstants.TABLE_TODO + " ( " +
                        DatabaseConstants.TODO_COLUMN_KEY + ", " +
                        DatabaseConstants.TODO_COLUMN_TASK + ", " +
                        DatabaseConstants.TODO_COLUMN_OWNER + ", " +
                        DatabaseConstants.TODO_COLUMN_PRIORITY + ", " +
                        DatabaseConstants.TODO_COLUMN_DONE + ", " +
                        DatabaseConstants.TODO_COLUMN_CREATED;
                if (due != null) {
                    query += ", " + DatabaseConstants.TODO_COLUMN_DUE;
                }
                if (participants != null) {
                    query += ", " + DatabaseConstants.TODO_COLUMN_PARTICIPANTS;
                }
                query += ") VALUES ( '" + key + "', '" + task + "', " + owner + ", '" + priority + "', " + false + ", '" + created + "'";
                if (due != null) {
                    query += ", '" + due + "'";
                }
                if (participants != null) {
                    query += ", '" + participants + "'";
                }
                query += " )";
                connection.update(query, res -> {
                    if (res.succeeded()) {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                        vertx.eventBus().send(AddressRegister.MQTT,
                                new JsonObject().put("username",
                                        messageJson.getString("username")).put("component", "todo").put("method", "post").put("key", key ));
                        message.reply(output);
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

    /**
     * Method to delete a to-do item with a given key
     * ---------------------------------------------
     * EventBus-Address: DELETE_TODO
     * ---------------------------------------------
     * Param:       JSON-Object with the following values
     * "username":     String username
     * "password":     String password
     * "key":          String key
     * ---------------------------------------------
     * Reply to EventBus:
     * JSON-Object with the following values
     * "httpCode":     int HTTP-Code
     * "serverCode":   int Server-Code
     */
    private void deleteTodo() {
        JsonObject output = new JsonObject();
        todoClient.getConnection(con -> {
            if (con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String key = messageJson.getString("key");
                String query = "DELETE FROM " + DatabaseConstants.TABLE_TODO + " WHERE " +
                        DatabaseConstants.TODO_COLUMN_KEY + " = '" + key + "'";
                connection.update(query, res -> {
                    if (res.succeeded()) {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                        message.reply(output);
                        vertx.eventBus().send(AddressRegister.MQTT,
                                new JsonObject().put("username",
                                        messageJson.getString("username")).put("component", "todo").put("method", "delete").put("key", key ));
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

    /**
     * Method to update a given to-do item
     * ---------------------------------------------
     * EventBus-Address: PATCH_TODO
     * ---------------------------------------------
     * Param:       JSON-Object with the following values
     * "username":     String username
     * "password":     String password
     * "key":          String key
     * "task":         String task
     * "owner":        int id
     * "priority":     String priority
     * "due":          String due
     * "done":         boolean done
     * "created":      String created
     * "participants"  String participants
     * ---------------------------------------------
     * Reply to EventBus:
     * JSON-Object with the following values
     * "httpCode":     int HTTP-Code
     * "serverCode":   int Server-Code
     */
    private void updateTodo() {
        JsonObject output = new JsonObject();
        todoClient.getConnection(con -> {
            if (con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String key = messageJson.getString("key");
                String task = messageJson.getString("task");
                int owner = messageJson.getInteger("owner");
                String priority = messageJson.getString("priority");
                if (task == null || priority == null ) {
                    output.put(CoreConstants.HTTP_CODE, HttpCodes.BAD_REQUEST);
                    output.put(CoreConstants.SERVER_CODE, ServerCodes.TODODATA_MISSING);
                    message.reply(output);
                }
                String due = messageJson.getString("due");
                String participants = messageJson.getString("partiipants");
                String query = "UPDATE " + DatabaseConstants.TABLE_TODO + " SET " +
                        DatabaseConstants.TODO_COLUMN_TASK + " = '" + task + "', " +
                        DatabaseConstants.TODO_COLUMN_OWNER + " = " + owner + ", " +
                        DatabaseConstants.TODO_COLUMN_PRIORITY + " = '" + priority + "'";
                if (due != null) {
                    query += ", " + DatabaseConstants.TODO_COLUMN_DUE + " = " + due;
                }
                if (participants != null) {
                    query += ", " + DatabaseConstants.TODO_COLUMN_PARTICIPANTS + " = " + participants;
                }
                query += " WHERE " + DatabaseConstants.TODO_COLUMN_KEY + " = '" + key + "'";
                connection.update(query, res -> {
                    if (res.succeeded()) {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                        message.reply(output);
                        vertx.eventBus().send(AddressRegister.MQTT,
                                new JsonObject().put("username",
                                        messageJson.getString("username")).put("component", "todo").put("method", "patch").put("key", key ));
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

    /**
     * Method to mark a to-do item as done with a given key
     * ---------------------------------------------
     * EventBus-Address: PATCH_TODO_DONE
     * ---------------------------------------------
     * Param:       JSON-Object with the following values
     * "username":     String username
     * "password":     String password
     * "key":          String key
     * ---------------------------------------------
     * Reply to EventBus:
     * JSON-Object with the following values
     * "httpCode":     int HTTP-Code
     * "serverCode":   int Server-Code
     */
    private void setTodoDone() {
        JsonObject output = new JsonObject();
        todoClient.getConnection(con -> {
            if (con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String key = messageJson.getString("key");
                String query = "UPDATE " + DatabaseConstants.TABLE_TODO + " SET " +
                        DatabaseConstants.TODO_COLUMN_DONE + " = " + true + " WHERE " + DatabaseConstants.TODO_COLUMN_KEY + " = '" + key + "'";
                connection.update(query, res -> {
                    if (res.succeeded()) {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                        message.reply(output);
                        vertx.eventBus().send(AddressRegister.MQTT,
                                new JsonObject().put("username",
                                        messageJson.getString("username")).put("component", "todo").put("method", "patch/done").put("key", key ));
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

    /**
     * Method to get detail information about a to-do item by a given key
     * ---------------------------------------------
     * EventBus-Address: GET_TODO_BY_KEY
     * ---------------------------------------------
     * Param:       JSON-Object with the following values
     * "key":          String key
     * ---------------------------------------------
     * Reply to EventBus:
     * JSON-Object with the following values
     * "httpCode":     int HTTP-Code
     * "key":          String key
     * "task":         String task
     * "owner":        int owner
     * "priority":     String priority
     * "due":          String due
     * "done":         boolean done
     * "created":      String created
     * "participants": String participants
     */
    private void getTodoByKey() {
        JsonObject output = new JsonObject();
        todoClient.getConnection(con -> {
            if (con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String key = messageJson.getString("key");
                String query = "SELECT * FROM " + DatabaseConstants.TABLE_TODO + " WHERE " +
                        DatabaseConstants.TODO_COLUMN_KEY + " = '" + key + "'";
                connection.query(query, res -> {
                    if (res.succeeded()) {
                        if(res.result().getRows().size() != 0) {
                            output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                            output.put("key", res.result().getRows().get(0).getString("key"));
                            output.put("task", res.result().getRows().get(0).getString("task"));
                            output.put("owner", res.result().getRows().get(0).getInteger("owner"));
                            output.put("priority", res.result().getRows().get(0).getString("priority"));
                            output.put("due", res.result().getRows().get(0).getString("due"));
                            output.put("done", res.result().getRows().get(0).getBoolean("done"));
                            output.put("created", res.result().getRows().get(0).getString("created"));
                            output.put("participants", res.result().getRows().get(0).getString("participants"));
                            message.reply(output);
                        } else {
                            output.put(CoreConstants.HTTP_CODE, HttpCodes.BAD_REQUEST);
                            message.reply(output);
                        }
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

    /**
     * Method to get detail information about all to-do items of a user by the given username
     * ---------------------------------------------
     * EventBus-Address: GET_TODO_BY_USER
     * ---------------------------------------------
     * Param:       JSON-Object with the following values
     * "username":     String username
     * ---------------------------------------------
     * Reply to EventBus:
     * JSON-Array with the following JsonObjects
     * {
     * "httpCode":     int HTTP-Code
     * }
     * {
     * key: to-do1
     * task: do something
     * }
     * {
     * key: to-do2
     * task: do something again
     * }
     * ...
     * ==> the first JsonObject contains only the HTTP-Code for the REST-Request
     * ==> all following JsonObjects contains one detail view of an to-do item
     */
    private void getTodoByUser() {
        JsonArray output = new JsonArray();
        todoClient.getConnection(con -> {
            if (con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String username = messageJson.getString("username");
                String queryUsername = "SELECT id FROM " + DatabaseConstants.TABLE_USER + " WHERE " +
                        DatabaseConstants.USER_COLUMN_USERNAME + " = '" + username + "'";
                connection.query(queryUsername, resUsername -> {
                    if(resUsername.succeeded()) {
                        int owner = resUsername.result().getRows().get(0).getInteger("id");
                        String query = "SELECT * FROM " + DatabaseConstants.TABLE_TODO + " WHERE " +
                                DatabaseConstants.TODO_COLUMN_OWNER + " = " + owner + " AND " + DatabaseConstants.TODO_COLUMN_DONE + " = " + false;
                        connection.query(query, res -> {
                            if (res.succeeded()) {
                                output.add(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.OK));
                                for (int i = 0; i < res.result().getRows().size(); i++){
                                    JsonObject todoList = new JsonObject();
                                    todoList.put("key", res.result().getRows().get(i).getString("key"));
                                    todoList.put("task", res.result().getRows().get(i).getString("task"));
                                    todoList.put("owner", res.result().getRows().get(i).getInteger("owner"));
                                    todoList.put("priority", res.result().getRows().get(i).getString("priority"));
                                    todoList.put("due", res.result().getRows().get(i).getString("due"));
                                    todoList.put("done", res.result().getRows().get(i).getBoolean("done"));
                                    todoList.put("created", res.result().getRows().get(i).getString("created"));
                                    todoList.put("participants", res.result().getRows().get(i).getString("participants"));
                                    output.add(todoList);
                                }
                                message.reply(output);
                            } else {
                                output.add(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR));
                                message.reply(output);
                            }

                        });
                    } else {
                        output.add(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR));
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.add(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR));
                message.reply(output);
            }
        });
        this.future.complete();
    }

}
