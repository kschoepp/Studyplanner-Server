package de.ingence.studyplanner.core.constants;


public class DatabaseConstants {

    //Table names
    public static final String TABLE_USER = '"' + "user" + '"';
    public static final String TABLE_TODO = '"' + "todo" + '"';
    public static final String TABLE_EXAM = '"' + "exam" + '"';
    public static final String TABLE_COURSE = '"' + "course" + '"';
    public static final String TABLE_CALENDAR = '"' + "calendar" + '"';
    public static final String TABLE_CONTACT = '"' + "contact" + '"';

    //Column names
    //|---Table user
    public static final String USER_COLUMN_ID = "id";
    public static final String USER_COLUMN_USERNAME = "username";
    public static final String USER_COLUMN_FIRSTNAME = "firstname";
    public static final String USER_COLUMN_LASTNAME = "lastname";
    public static final String USER_COLUMN_EMAIL = "email";
    public static final String USER_COLUMN_PASSWORD = "password";
    public static final String USER_COLUMN_BIRTHDAY = "birthday";
    public static final String USER_COLUMN_LOCATION = "location";
    public static final String USER_COLUMN_PICTURE = "picture";
    public static final String USER_COLUMN_ACTIVE = "active";
    public static final String USER_COLUMN_ACTIVATION_KEY = "activation_key";

    //---Table to-do
    public static final String TODO_COLUMN_KEY = "key";
    public static final String TODO_COLUMN_TASK = "task";
    public static final String TODO_COLUMN_OWNER = "owner";
    public static final String TODO_COLUMN_PRIORITY = "priority";
    public static final String TODO_COLUMN_DUE = "due";
    public static final String TODO_COLUMN_DONE = "done";
    public static final String TODO_COLUMN_CREATED = "created";
    public static final String TODO_COLUMN_PARTICIPANTS = "participants";


    //---Table exam
    public static final String EXAM_COLUMN_KEY = "key";
    public static final String EXAM_COLUMN_TITLE = "title";
    public static final String EXAM_COLUMN_OWNER = "owner";
    public static final String EXAM_COLUMN_DATE_START = "date_start";
    public static final String EXAM_COLUMN_DATE_END = "date_end";
    public static final String EXAM_COLUMN_LOCATION = "location";


    //---Table course
    public static final String COURSE_COLUMN_KEY = "key";
    public static final String COURSE_COLUMN_TITLE = "title";
    public static final String COURSE_COLUMN_OWNER = "owner";
    public static final String COURSE_COLUMN_DAY = "day";
    public static final String COURSE_COLUMN_DATE_START = "date_start";
    public static final String COURSE_COLUMN_DATE_END = "date_end";
    public static final String COURSE_COLUMN_LOCATION = "location";


    //---Table calender
    public static final String CALENDER_COLUMN_KEY = "key";
    public static final String CALENDER_COLUMN_TOPIC = "topic";
    public static final String CALENDER_COLUMN_OWNER = "owner";
    public static final String CALENDER_COLUMN_DATE_START = "date_start";
    public static final String CALENDER_COLUMN_DATE_END = "date_end";
    public static final String CALENDER_COLUMN_LOCATION = "location";
    public static final String CALENDER_COLUMN_PARTICIPANTS = "participants";
    public static final String CALENDER_COLUMN_CREATED = "created";


    //---Table contact
    public static final String CONTACT_COLUMN_ID = "id";
    public static final String CONTACT_COLUMN_CREATOR = "creator";
    public static final String CONTACT_COLUMN_USER_ONE = "user_one";
    public static final String CONTACT_COLUMN_USER_TWO = "user_two";
    public static final String CONTACT_COLUMN_CONFIRMED = "confirmed";

}
