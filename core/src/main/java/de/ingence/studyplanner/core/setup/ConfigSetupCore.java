package de.ingence.studyplanner.core.setup;



import de.ingence.studyplanner.core.constants.CoreConstants;
import de.ingence.studyplanner.core.constants.Errors;
import de.ingence.studyplanner.core.constants.Messages;
import de.ingence.studyplanner.core.constants.SetupConstants;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;


public class ConfigSetupCore {

    public static boolean setup() throws Exception {
        //server.properties
        File propertiesFile = new File(CoreConstants.CONFIG_DIR_PATH + "/" +CoreConstants.CONFIG_FILE_CORE);
        if(propertiesFile.exists()) {
            System.out.println(Messages.curDate() + Messages.INFORMATION + Messages.FILESETUP_PROPERTIESFILE_ALREADY_EXISTS);
            return false;
        } else {
            File dir = new File(CoreConstants.CONFIG_DIR_PATH);
            if(!dir.exists()) {
                dir.mkdir();
            }
            System.out.println(Messages.curDate() + Messages.INFORMATION + Messages.FILESETUP_PROPERTIESFILE_START_SETUP);
            if(propertiesFile.createNewFile()) {
                System.out.println(Messages.curDate() + Messages.INFORMATION + Messages.FILESETUP_PROPERTIESFILE_CREATED);
                FileWriter fileWriter = new FileWriter(CoreConstants.CONFIG_DIR_PATH + "/" +CoreConstants.CONFIG_FILE_CORE);
                BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
                bufferedWriter.write(SetupConstants.PROPERTIES_VALUES);
                bufferedWriter.close();
                System.out.println(Messages.curDate() + Messages.INFORMATION + Messages.FILESETUP_PROPERTIESFILE_INIT);
                return true;
            } else {
                throw new Exception(Errors.curDate() + Errors.ERROR + Errors.FILESETUP_PROPERTIESFILE_CREATION_FILED);
            }
        }

    }

}

