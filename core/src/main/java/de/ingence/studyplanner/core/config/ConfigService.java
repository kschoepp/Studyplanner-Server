package de.ingence.studyplanner.core.config;


import de.ingence.studyplanner.core.constants.CoreConstants;
import io.vertx.core.json.JsonObject;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.util.Properties;


public class ConfigService {

    public static JsonObject getDatabaseConfig() throws Exception {
        JsonObject config = new JsonObject();
        config.put("host", ConfigService.getDatabaseHost());
        config.put("port", ConfigService.getDatabasePort());
        config.put("database", ConfigService.getDatabaseName());
        config.put("username", ConfigService.getDatabaseUsername());
        config.put("password", ConfigService.getDatabasePassword());
        return config;
    }


    public static String getDatabaseHost() throws Exception {
        String output = getConfig("database_host");
        if(output == null || output.isEmpty()) {
            output = CoreConstants.DATABASE_HOST;
            //TODO Sout + Log: Database Host empty, using default value
        }
        return output;
    }

    public static int getDatabasePort() throws Exception {
        String value = getConfig("database_port");
        int output = CoreConstants.DATABASE_PORT;
        if (value != null && !value.isEmpty()) {
            output = Integer.parseInt(value);
        } else {
            //TODO Sout + Log: Database Port empty, using default value
        }
        return output;
    }

    public static boolean getDatabaseSSL() throws Exception {
        String value = getConfig("database_ssl");
        return value != null && !value.isEmpty() && value.equals("true");
    }

    public static String getDatabaseName() throws Exception {
        String output = getConfig("database_name");
        if(output == null || output.isEmpty()) {
            output = CoreConstants.DATABASE_NAME;
            //TODO Sout + Log: Database Name empty, using default value
        }
        return output;
    }

    public static String getDatabaseUsername() throws Exception {
        String output = getConfig("database_username");
        if(output == null || output.isEmpty()) {
            //TODO Exception + Log: Database Username empty, unable to connect
            return null;
        }
        return output;
    }

    public static String getDatabasePassword() throws Exception {
        String output = getConfig("database_password");
        if(output == null || output.isEmpty()) {
            //TODO Exception + Log: Database Password empty, unable to connect
            return null;
        }
        return output;
    }

    public static String getConfig(String param) throws Exception {
        Properties p = new Properties();
        BufferedInputStream b = new BufferedInputStream(new FileInputStream(CoreConstants.CONFIG_DIR_PATH + "/" + CoreConstants.CONFIG_FILE_CORE));
        p.load(b);
        b.close();
        return p.getProperty(param);
    }

}
