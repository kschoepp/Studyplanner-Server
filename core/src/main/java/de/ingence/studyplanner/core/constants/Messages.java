package de.ingence.studyplanner.core.constants;



public class Messages extends ConsoleStyles {

    //FileSetup messages
    public static final String FILESETUP_PROPERTIESFILE_ALREADY_EXISTS = "Skipping creating <core.properties> file. File already exists...";
    public static final String FILESETUP_PROPERTIESFILE_START_SETUP = "Creating <core.properties> file...";
    public static final String FILESETUP_PROPERTIESFILE_CREATED = "File <core.properties> created";
    public static final String FILESETUP_PROPERTIESFILE_INIT = "File <core.properties> initializied";


}

