package de.ingence.studyplanner.core.constants;


public class CoreConstants {

    //|---Database default values
    public static final String DATABASE_HOST = "localhost";
    public static final int DATABASE_PORT = 5432;
    public static final String DATABASE_NAME = "studyplanner";
    public static final int DATABASE_TIMEOUT = 128;
    //File paths
    public static final String CONFIG_DIR_PATH = "./config";
    public static final String CONFIG_FILE_CORE = "core.properties";
    public static final String DATABASE_SETUP_DIR_PATH = "./sql";
    public static final String DATABASE_SETUP_FILE = "PostgreSQL-Setup.sql";
    //CodeLabels
    public static final String HTTP_CODE = "httpCode";
    public static final String SERVER_CODE = "serverCode";

}
