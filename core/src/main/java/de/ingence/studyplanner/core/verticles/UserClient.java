package de.ingence.studyplanner.core.verticles;

import de.ingence.studyplanner.core.config.ConfigService;
import de.ingence.studyplanner.core.constants.*;
import de.ingence.studyplanner.core.utils.KeyGenerator;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.asyncsql.PostgreSQLClient;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;

import static de.ingence.studyplanner.core.constants.ServerCodes.USERDATA_MISSING;


public class UserClient extends AbstractVerticle {

    //Global variables
    private Future<Void> future;
    private int method;
    //|---VERT.X Objects
    private Message<Object> message;
    private SQLClient userClient;

    public UserClient(int method, Message<Object> message) {
        this.method = method;
        this.message = message;
    }

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        this.future = startFuture;
        //Creating database client
        JsonObject clientConfig = ConfigService.getDatabaseConfig();
        this.userClient = PostgreSQLClient.createShared(vertx, clientConfig);

        switch (method) {
            //|---GET
            case MethodRegister.GET_ALL_USER:
                this.getAllUser();
                break;
            case MethodRegister.GET_USER_LOGIN:
                this.validateLogin();
                break;
            case MethodRegister.GET_USER_DETAILS:
                this.getUserDetails();
                break;
            case MethodRegister.GET_USER_REGISTER_USERNAME:
                this.checkUsername();
                break;
            case MethodRegister.GET_USER_BY_ID:
                this.getUserByID();
                break;
            case MethodRegister.GET_USER_BY_ACT_KEY:
                this.getUserByActivationKey();
                break;
            //|---POST
            case MethodRegister.POST_USER_REGISTER:
                this.registerUser();
                break;
            //|---PATCH
            case MethodRegister.PATCH_USER:
                this.updateUser();
                break;
            case MethodRegister.PATCH_USER_PASSWORD:
                this.updatePassword();
                break;
            case MethodRegister.PATCH_USER_ACTIVE:
                this.activateUser();
                break;
            //|---DELETE
            case MethodRegister.DELETE_USER:
                this.deleteUser();
                break;
            default:
                break;
        }
    }

    private void registerUser() {
        JsonObject output = new JsonObject();
        userClient.getConnection(con -> {
            if(con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String username = messageJson.getString("username");
                String firstname = messageJson.getString("firstname");
                String lastname = messageJson.getString("lastname");
                String email = messageJson.getString("email");
                String password = messageJson.getString("password");
                if(username == null || firstname == null || lastname == null || email == null || password == null) {
                    output.put(CoreConstants.HTTP_CODE, HttpCodes.BAD_REQUEST);
                    output.put(CoreConstants.SERVER_CODE, ServerCodes.USERDATA_MISSING);
                    message.reply(output);
                } else {
                    String birthday = messageJson.getString("birthday");
                    String location = messageJson.getString("location");
                    String picture = messageJson.getString("picture");
                    String querySearchUser = "SELECT * FROM " + DatabaseConstants.TABLE_USER + " WHERE " +
                            DatabaseConstants.USER_COLUMN_USERNAME + " = '" + username + "' AND " +
                            DatabaseConstants.USER_COLUMN_FIRSTNAME + " = '" + firstname + "' AND " +
                            DatabaseConstants.USER_COLUMN_LASTNAME + " = '" + lastname + "' AND " +
                            DatabaseConstants.USER_COLUMN_EMAIL + " = '" + email + "'";
                    connection.query(querySearchUser, resUser -> {
                        if (resUser.succeeded()) {
                            if (resUser.result().getResults().size() != 0) {
                                output.put(CoreConstants.HTTP_CODE, HttpCodes.BAD_REQUEST);
                                output.put(CoreConstants.SERVER_CODE, ServerCodes.USER_EXISTS);
                                message.reply(output);
                            } else {
                                String querySearchUsername = "SELECT * FROM " + DatabaseConstants.TABLE_USER + " WHERE " +
                                        DatabaseConstants.USER_COLUMN_USERNAME + " = '" + username + "'";
                                connection.query(querySearchUsername, resUsername-> {
                                    if(resUsername.succeeded()) {
                                        if (resUsername.result().getResults().size() != 0) {
                                            output.put(CoreConstants.HTTP_CODE, HttpCodes.BAD_REQUEST);
                                            output.put(CoreConstants.SERVER_CODE, ServerCodes.USERNAME_EXISTS);
                                            message.reply(output);
                                        } else {
                                            String query = "INSERT INTO " + DatabaseConstants.TABLE_USER + " (" +
                                                    DatabaseConstants.USER_COLUMN_USERNAME + ", " +
                                                    DatabaseConstants.USER_COLUMN_FIRSTNAME + ", " +
                                                    DatabaseConstants.USER_COLUMN_LASTNAME + ", " +
                                                    DatabaseConstants.USER_COLUMN_EMAIL + ", " +
                                                    DatabaseConstants.USER_COLUMN_PASSWORD;
                                            if (birthday != null) {
                                                query += ", " + DatabaseConstants.USER_COLUMN_BIRTHDAY;
                                            }
                                            if (location != null) {
                                                query += ", " + DatabaseConstants.USER_COLUMN_LOCATION;
                                            }
                                            if (picture != null) {
                                                query += ", " + DatabaseConstants.USER_COLUMN_PICTURE;
                                            }
                                            query += ", active, " +
                                                    DatabaseConstants.USER_COLUMN_ACTIVATION_KEY +
                                                    ") VALUES (" +
                                                    "'" + username + "', '" + firstname + "', '" + lastname + "', '" + email + "', '" + password + "'";
                                            if (birthday != null) {
                                                query += ", '" + birthday + "'";
                                            }
                                            if (location != null) {
                                                query += ", '" + location + "'";
                                            }
                                            if (picture != null) {
                                                query += ", '" + picture + "'";
                                            }
                                            query += ", false, " + "'" +
                                                    KeyGenerator.generateKey(username) + "'" +
                                                    ");";
                                            connection.update(query, res -> {
                                                if (res.succeeded()) {
                                                    String queryID = "SELECT activation_key FROM " + DatabaseConstants.TABLE_USER + " WHERE " +
                                                            DatabaseConstants.USER_COLUMN_USERNAME + " = '" + username + "'";
                                                    connection.query(queryID, resID -> {
                                                        if(resID.succeeded()) {
                                                            output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                                                            output.put(DatabaseConstants.USER_COLUMN_ACTIVATION_KEY, resID.result().getRows().get(0).getString("activation_key"));
                                                            message.reply(output);
                                                        } else {
                                                            output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                                                            message.reply(output);
                                                        }
                                                    });
                                                } else {
                                                    output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                                                    message.reply(output);
                                                }
                                            });
                                        }
                                    } else {
                                        output.put(CoreConstants.HTTP_CODE,HttpCodes.INTERNAL_SERVER_ERROR);
                                        message.reply(output);
                                    }
                                });
                            }
                        } else {
                            output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                            message.reply(output);
                        }
                    });
                }
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

    private void deleteUser() {
        JsonObject output = new JsonObject();
        userClient.getConnection(con -> {
            if(con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                int id = messageJson.getInteger("id");
                String query = "DELETE FROM " + DatabaseConstants.TABLE_USER + " WHERE " +
                        DatabaseConstants.USER_COLUMN_ID + " = " + id;

                connection.update(query, res -> {
                    connection.close();
                    if (res.succeeded()) {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                        message.reply(output);
                        vertx.eventBus().send(AddressRegister.MQTT,
                                new JsonObject().put("username",
                                        messageJson.getString("username")).put("component", "user").put("method", "delete").put("id", id ));
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

    /**
     * Method to validate if given user data are valid
     * ---------------------------------------------
     * EventBus-Address: GET_USER_LOGIN
     * ---------------------------------------------
     * Param:       JSON-Object with the following values
     *              "username":     String username
     *              "password":     String password
     * ---------------------------------------------
     * Reply to EventBus:
     *              JSON-Object with the following values
     *              "httpCode":     int HTTP-Code
     *              "serverCode":   int Server-Code
     */
    private void validateLogin() {
        JsonObject output = new JsonObject();
        userClient.getConnection(con -> {
            if(con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String username = messageJson.getString("username");
                String password = messageJson.getString("password");
                String queryUsername = "SELECT username FROM " + DatabaseConstants.TABLE_USER + " WHERE " +
                        DatabaseConstants.USER_COLUMN_USERNAME + " = '" + username + "'";
                connection.query(queryUsername, resUsername-> {
                    if(resUsername.succeeded()) {
                        if (resUsername.result().getResults().size() == 0) {
                            output.put(CoreConstants.HTTP_CODE, HttpCodes.BAD_REQUEST);
                            output.put(CoreConstants.SERVER_CODE, ServerCodes.USERNAME_NOT_EXISTS);
                            message.reply(output);
                        } else {
                            String query = "SELECT id, username, password FROM " + DatabaseConstants.TABLE_USER + " WHERE " +
                                    DatabaseConstants.USER_COLUMN_USERNAME + " = '" + username + "' AND " +
                                    DatabaseConstants.USER_COLUMN_PASSWORD + " = '" + password + "'";
                            connection.query(query, res -> {
                                if ( res.succeeded()) {
                                    if (res.result().getResults().size() != 0) {
                                        output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                                        message.reply(output);
                                    } else {
                                        output.put(CoreConstants.HTTP_CODE, HttpCodes.BAD_REQUEST);
                                        output.put(CoreConstants.SERVER_CODE, ServerCodes.PASSWORD_WRONG);
                                        message.reply(output);
                                    }
                                } else {
                                    output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                                    message.reply(output);
                                }
                            });
                        }
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

    /**
     * Method to validate if a given username is available
     * ---------------------------------------------
     * EventBus-Address: GET_USER_REGISTER_USERNAME
     * ---------------------------------------------
     * Param:       JSON-Object with the following values
     *              "username":     String username
     * ---------------------------------------------
     * Reply to EventBus:
     *              JSON-Object with the following values
     *              "httpCode":     int HTTP-Code
     *              "serverCode":   int Server-Code
     */
    void checkUsername() {
        JsonObject output = new JsonObject();
        userClient.getConnection(con -> {
            if (con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String username = messageJson.getString("username");
                String querySearchUsername = "SELECT * FROM " + DatabaseConstants.TABLE_USER + " WHERE " +
                        DatabaseConstants.USER_COLUMN_USERNAME + " = '" + username + "'";
                connection.query(querySearchUsername, result -> {
                    if (result.succeeded()) {
                        if(result.result().getResults().size() == 0){
                            output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                            output.put(CoreConstants.SERVER_CODE, ServerCodes.USERNAME_NOT_EXISTS);
                            message.reply(output);
                        } else {
                            output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                            output.put(CoreConstants.SERVER_CODE, ServerCodes.USERNAME_EXISTS);
                            message.reply(output);
                        }

                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

    private void updateUser() {
        JsonObject output = new JsonObject();
        userClient.getConnection(con -> {
            if(con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                int id = messageJson.getInteger("id");
                String username = messageJson.getString("username");
                String firstname = messageJson.getString("firstname");
                String lastname = messageJson.getString("lastname");
                String email = messageJson.getString("email");
                if (username == null || firstname == null || lastname == null || email == null) {
                    output.put(CoreConstants.HTTP_CODE, HttpCodes.BAD_REQUEST);
                    output.put(CoreConstants.SERVER_CODE, ServerCodes.USERDATA_MISSING);
                    message.reply(USERDATA_MISSING);
                } else {
                    String birthday = messageJson.getString("birthday");
                    String location = messageJson.getString("location");
                    String picture = messageJson.getString("picture");
                    String query = "UPDATE " + DatabaseConstants.TABLE_USER + " SET " +
                            DatabaseConstants.USER_COLUMN_USERNAME + " = '" + username + "' ," +
                            DatabaseConstants.USER_COLUMN_FIRSTNAME + " = '" + firstname + "' , " +
                            DatabaseConstants.USER_COLUMN_LASTNAME + " = '" + lastname + "' , " +
                            DatabaseConstants.USER_COLUMN_EMAIL + " = '" + email + "'";
                    if (birthday != null) {
                        query += ", " + DatabaseConstants.USER_COLUMN_BIRTHDAY + " = '" + birthday + "'";
                    }
                    if (location != null) {
                        query += ", " + DatabaseConstants.USER_COLUMN_LOCATION + " = '" + location + "'";
                    }
                    if (picture != null) {
                        query += ", " + DatabaseConstants.USER_COLUMN_PICTURE + " = '" + picture + "'";
                    }
                    query += " WHERE " + DatabaseConstants.CONTACT_COLUMN_ID + " = " + id;
                    connection.update(query, res -> {
                        if (res.succeeded()) {
                            output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                            message.reply(output);
                            vertx.eventBus().send(AddressRegister.MQTT,
                                    new JsonObject().put("username",
                                            messageJson.getString("username")).put("component", "user").put("method", "patch").put("id", id ));
                        } else {
                            output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                            message.reply(output);
                        }
                    });
                }
                connection.close();
            } else{
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

    private void updatePassword() {
        JsonObject output = new JsonObject();
        userClient.getConnection(con -> {
            if(con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                int id = messageJson.getInteger("id");
                String password = messageJson.getString("password");
                String queryUsername = "SELECT * FROM " + DatabaseConstants.TABLE_USER + " WHERE " +
                        DatabaseConstants.USER_COLUMN_ID + " = " + id;
                connection.query(queryUsername, resUsername -> {
                    if(resUsername.succeeded()) {
                        String query = "UPDATE " + DatabaseConstants.TABLE_USER + " SET " +
                                DatabaseConstants.USER_COLUMN_PASSWORD + " = '" + password + "' WHERE " +
                                DatabaseConstants.USER_COLUMN_ID + " = " + id;
                        connection.update(query, res -> {
                            if (res.succeeded()) {
                                output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                                message.reply(output);
                                vertx.eventBus().send(AddressRegister.MQTT,
                                        new JsonObject().put("username",
                                                messageJson.getString("username")).put("component", "password").put("method", "patch").put("id", id ));
                            } else {
                                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                                message.reply(output);
                            }
                        });
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else{
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

    private void activateUser(){
        JsonObject output = new JsonObject();
        userClient.getConnection(con -> {
            if(con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String activationKey = messageJson.getString("activation_key");
                String query = "UPDATE " + DatabaseConstants.TABLE_USER + " SET " +
                        DatabaseConstants.USER_COLUMN_ACTIVE + " = " + true + " WHERE " +
                        DatabaseConstants.USER_COLUMN_ACTIVATION_KEY + " = '" + activationKey + "'";
                        connection.update(query, res -> {
                            if (res.succeeded()) {
                                output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                                message.reply(output);
                                vertx.eventBus().send(AddressRegister.MQTT,
                                        new JsonObject().put("username",
                                                messageJson.getString("username")).put("component", "password").put("method", "patch"));
                            } else {
                                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                                message.reply(output);
                            }
                        });
                        connection.close();
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
        this.future.complete();
    }

    private void getUserByActivationKey() {
        JsonObject output = new JsonObject();
        userClient.getConnection(con -> {
            if(con.succeeded()){
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String activationKey = messageJson.getString("activation_key");
                String query = "SELECT username, password FROM " + DatabaseConstants.TABLE_USER + " WHERE " +
                        DatabaseConstants.USER_COLUMN_ACTIVATION_KEY + " = '" + activationKey + "'";
                connection.query(query, res -> {
                    if(res.succeeded()){
                        if(res.result().getRows().size() != 0) {
                            output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                            output.put(DatabaseConstants.USER_COLUMN_USERNAME, res.result().getRows().get(0).getString("username"));
                            output.put(DatabaseConstants.USER_COLUMN_PASSWORD, res.result().getRows().get(0).getString("password"));
                            message.reply(output);
                        } else {
                            output.put(CoreConstants.HTTP_CODE, HttpCodes.BAD_REQUEST);
                            message.reply(output);
                        }
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            }
        });
        this.future.complete();
    }

    private void getUserDetails(){
        JsonObject output = new JsonObject();
        userClient.getConnection(con -> {
            if(con.succeeded()){
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String username = messageJson.getString("username");
                String query = "SELECT * FROM " + DatabaseConstants.TABLE_USER + " WHERE " +
                        DatabaseConstants.USER_COLUMN_USERNAME + " = '" + username + "'";
                connection.query(query, res -> {
                    if(res.succeeded()){
                        if(res.result().getRows().size() != 0) {
                            output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                            output.put("username", username);
                            output.put("id", res.result().getRows().get(0).getInteger("id"));
                            output.put("firstname", res.result().getRows().get(0).getString("firstname"));
                            output.put("lastname", res.result().getRows().get(0).getString("lastname"));
                            output.put("email", res.result().getRows().get(0).getString("email"));
                            output.put("birthday", res.result().getRows().get(0).getString("birthday"));
                            output.put("location", res.result().getRows().get(0).getString("location"));
                            output.put("picture", res.result().getRows().get(0).getString("picture"));
                            message.reply(output);
                        }else {
                            output.put(CoreConstants.HTTP_CODE, HttpCodes.BAD_REQUEST);
                            message.reply(output);
                        }
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            }
        });
        this.future.complete();
    }

    private void getUserByID(){
        JsonObject output = new JsonObject();
        userClient.getConnection(con -> {
            if(con.succeeded()){
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                int id = messageJson.getInteger("id");
                String query = "SELECT * FROM " + DatabaseConstants.TABLE_USER + " WHERE " +
                        DatabaseConstants.USER_COLUMN_ID + " = " + id;
                connection.query(query, res -> {
                    if(res.succeeded()){
                        if(res.result().getRows().size() != 0){
                            output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                            output.put("username", res.result().getRows().get(0).getString("username"));
                            output.put("firstname", res.result().getRows().get(0).getString("firstname"));
                            output.put("lastname", res.result().getRows().get(0).getString("lastname"));
                            output.put("email", res.result().getRows().get(0).getString("email"));
                            output.put("birthday", res.result().getRows().get(0).getString("birthday"));
                            output.put("location", res.result().getRows().get(0).getString("location"));
                            output.put("picture", res.result().getRows().get(0).getString("picture"));
                            message.reply(output);
                        }else {
                            output.put(CoreConstants.HTTP_CODE, HttpCodes.BAD_REQUEST);
                            message.reply(output);
                        }

                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);

                    }
                });
                connection.close();
            }
        });
        this.future.complete();
    }

    private void getAllUser(){
        JsonArray ja = new JsonArray();
        userClient.getConnection(con -> {
            if(con.succeeded()){
                SQLConnection connection = con.result();
                String query = "SELECT * FROM " + DatabaseConstants.TABLE_USER;
                connection.query(query, res -> {
                    if(res.succeeded()) {
                        if (res.result().getRows().size() != 0) {
                            for (int i = 0; i < res.result().getRows().size(); i++) {
                                JsonObject output = new JsonObject();
                                output.put("username", res.result().getRows().get(i).getString("username"));
                                output.put("password", res.result().getRows().get(i).getString("password"));
                                ja.add(output);
                            }
                        }
                    }
                    message.reply(ja);
                });
                connection.close();
            }
        });
        this.future.complete();
    }

}
