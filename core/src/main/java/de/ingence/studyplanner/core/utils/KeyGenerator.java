package de.ingence.studyplanner.core.utils;

import com.github.mauricio.async.db.util.HexCodec;

import javax.xml.bind.DatatypeConverter;
import java.security.MessageDigest;


public class KeyGenerator {

    public static String generateKey(String value) {
        double rndNumber = Math.floor(Math.random() * (Math.random() - Math.random() + 1)) + Math.random();
        String key = value + rndNumber;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            String eKey = DatatypeConverter.printHexBinary(md.digest(key.getBytes()));
            return eKey;
        } catch (Exception e) {
            return key;
        }
    }

}
