package de.ingence.studyplanner.core.constants;


public class MethodRegister {

    //|---GET

    public static final int GET_USER_LOGIN = 101;
    public static final int GET_USER_DETAILS = 102;
    public static final int GET_USER_REGISTER_USERNAME = 103;
    public static final int GET_USER_BY_ID = 104;
    public static final int GET_USER_BY_ACT_KEY = 105;
    public static final int GET_TODO_BY_KEY = 106;
    public static final int GET_TODO_BY_USER = 107;
    public static final int GET_CALENDAR_BY_KEY = 108;
    public static final int GET_CALENDAR_BY_USER = 109;
    public static final int GET_CALENDAR_BY_DATE = 110;
    public static final int GET_COURSE_BY_KEY = 111;
    public static final int GET_COURSE_BY_USER = 112;
    public static final int GET_EXAM_BY_KEY = 113;
    public static final int GET_EXAM_BY_USER = 114;
    public static final int GET_CONTACT_BY_ID = 115;
    public static final int GET_CONTACT_BY_USER = 116;
    public static final int GET_ALL_USER = 117;
    //|---POST
    public static final int POST_USER_REGISTER = 201;
    public static final int POST_TODO = 202;
    public static final int POST_COURSE = 203;
    public static final int POST_EXAM = 204;
    public static final int POST_CALENDAR = 205;
    public static final int POST_CONTACT = 206;
    //|---PATCH
    public static final int PATCH_USER = 301;
    public static final int PATCH_USER_PASSWORD = 302;
    public static final int PATCH_USER_ACTIVE = 303;
    public static final int PATCH_TODO = 304;
    public static final int PATCH_TODO_DONE = 305;
    public static final int PATCH_COURSE = 306;
    public static final int PATCH_EXAM = 307;
    public static final int PATCH_CALENDAR = 308;
    public static final int PATCH_CONTACT_CONFIRM = 309;
    //|---DELETE
    public static final int DELETE_USER = 401;
    public static final int DELETE_TODO = 402;
    public static final int DELETE_COURSE = 403;
    public static final int DELETE_EXAM = 404;
    public static final int DELETE_CALENDAR = 405;
    public static final int DELETE_CONTACT = 406;

}
