package de.ingence.studyplanner.core;
import de.ingence.studyplanner.core.setup.ConfigSetupCore;
import io.vertx.core.Vertx;


public class Main {

    public static void main(String[] args) {
        try {
            if(ConfigSetupCore.setup()) {
                System.exit(1);
            }
            //Creating VERT.X Instance
            Vertx vertx = Vertx.vertx();
            //Deploy verticle
            vertx.deployVerticle(new Core());
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
