package de.ingence.studyplanner.core.verticles;

import de.ingence.studyplanner.core.config.ConfigService;
import de.ingence.studyplanner.core.constants.*;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.asyncsql.PostgreSQLClient;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;


public class ContactClient extends AbstractVerticle {

    //Global variables
    private Future<Void> future;
    private int method;
    //|---VERT.X Objects
    private Message<Object> message;
    private SQLClient contactClient;

    public ContactClient(int method, Message<Object> message) {
        this.method = method;
        this.message = message;
    }

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        this.future = startFuture;
        //Creating database client
        JsonObject clientConfig = ConfigService.getDatabaseConfig();
        this.contactClient = PostgreSQLClient.createShared(vertx, clientConfig);

        switch (method) {
            //|---GET
            case MethodRegister.GET_CONTACT_BY_ID:
                this.getContactByID();
                break;
            case MethodRegister.GET_CONTACT_BY_USER:
                this.getContactByUser();
                break;
            //|---POST
            case MethodRegister.POST_CONTACT:
                this.createContact();
                break;
            //|---PATCH
            case MethodRegister.PATCH_CONTACT_CONFIRM:
                this.confirmContact();
                break;
            //|---DELETE
            case MethodRegister.DELETE_CONTACT:
                this.deleteContact();
                break;
            default:
                break;
        }
    }

    /**
     * Method to create a new contact relationship
     * ---------------------------------------------
     * EventBus-Address: POST_CONTACT
     * ---------------------------------------------
     * Param:       JSON-Object with the following values
     *              "username":     String username
     *              "password":     String password
     *              "owner":        int user_one
     *              "contact"       int user_two
     * ---------------------------------------------
     * Reply to EventBus:
     *              JSON-Object with the following values
     *              "httpCode":     int HTTP-Code
     *              "serverCode":   int Server-Code
     *              "id"            int id
     */
    private void createContact() {
        JsonObject output = new JsonObject();
        contactClient.getConnection(con -> {
            if(con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                int creator = Integer.parseInt(messageJson.getString("creator"));
                int user_one = Integer.parseInt(messageJson.getString("user_one"));
                int user_two = Integer.parseInt(messageJson.getString("user_two"));

                String query = "INSERT INTO " + DatabaseConstants.TABLE_CONTACT + " (" +
                        DatabaseConstants.CONTACT_COLUMN_CREATOR + ", " +
                        DatabaseConstants.CONTACT_COLUMN_USER_ONE + ", " +
                        DatabaseConstants.CONTACT_COLUMN_USER_TWO + ", " +
                        DatabaseConstants.CONTACT_COLUMN_CONFIRMED + ") VALUES (" +
                        creator + ", " + user_one + ", " + user_two + ", " + false + " )";
                connection.update(query, res -> {
                    if(res.succeeded()){
                        String queryID = "SELECT id FROM " + DatabaseConstants.TABLE_CONTACT + " WHERE " +
                                DatabaseConstants.CONTACT_COLUMN_USER_ONE + " = " + user_one + " AND " +
                                DatabaseConstants.CONTACT_COLUMN_USER_TWO + " = " + user_two;
                        connection.query(queryID, resID -> {
                            if(resID.succeeded()) {
                                output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                                output.put("id", resID.result().getRows().get(0).getInteger("id"));
                                message.reply(output);
                                vertx.eventBus().send(AddressRegister.MQTT,
                                        new JsonObject().put("username",
                                                messageJson.getString("username")).put("component", "contact").put("method", "post").put("key", resID.result().getRows().get(0).getInteger("id") ));
                            } else {
                                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                                message.reply(output);
                            }
                        });
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

    private void deleteContact() {
        JsonObject output = new JsonObject();
        contactClient.getConnection(con -> {
            if (con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                int id = messageJson.getInteger("id");
                String query = "DELETE FROM " + DatabaseConstants.TABLE_CONTACT + " WHERE " +
                        DatabaseConstants.CONTACT_COLUMN_ID + " = " + id;
                connection.update(query, res-> {
                    if(res.succeeded()){
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                        vertx.eventBus().send(AddressRegister.MQTT, new JsonObject().put("username", messageJson.getString("username")).put("component", "contact").put("method", "delete").put("id", id));
                        message.reply(output);
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

    private void confirmContact() {
        JsonObject output = new JsonObject();
        contactClient.getConnection(con -> {
            if (con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String id = messageJson.getString("id");
                String query = "UPDATE " + DatabaseConstants.TABLE_CONTACT + " SET " +
                        DatabaseConstants.CONTACT_COLUMN_CONFIRMED + " = " + true + " WHERE " +
                        DatabaseConstants.CONTACT_COLUMN_ID + " = " + id;
                connection.update(query, res-> {
                    if(res.succeeded()){
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                        vertx.eventBus().send(AddressRegister.MQTT,
                                new JsonObject().put("username",
                                        messageJson.getString("username")).put("component", "contact").put("method", "patch").put("id", id));
                        message.reply(output);
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

    private void getContactByID() {
        JsonObject output = new JsonObject();
        contactClient.getConnection(con -> {
            if (con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                int id = messageJson.getInteger("id");
                String query = "SELECT * FROM " + DatabaseConstants.TABLE_CONTACT + " WHERE " +
                        DatabaseConstants.CONTACT_COLUMN_ID + " = " + id + "";
                connection.query(query, res -> {
                    if (res.succeeded()) {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                        output.put("id", res.result().getRows().get(0).getInteger("id"));
                        output.put("creator", res.result().getRows().get(0).getInteger("creator"));
                        output.put("user_one", res.result().getRows().get(0).getInteger("user_one"));
                        output.put("user_two", res.result().getRows().get(0).getInteger("user_two"));
                        output.put("confirmed", res.result().getRows().get(0).getBoolean("confirmed"));
                        message.reply(output);
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

    private void getContactByUser() {
        JsonObject output = new JsonObject();
        JsonArray ja = new JsonArray();
        contactClient.getConnection(con -> {
            if (con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String username = messageJson.getString("username");
                String queryUsername = "SELECT id FROM " + DatabaseConstants.TABLE_USER + " WHERE " +
                        DatabaseConstants.USER_COLUMN_USERNAME + " = '" + username + "'";
                connection.query(queryUsername, resUsername -> {
                    if(resUsername.succeeded()) {
                        int owner = resUsername.result().getRows().get(0).getInteger("id");
                        String query = "SELECT * FROM " + DatabaseConstants.TABLE_CONTACT + " WHERE " +
                                DatabaseConstants.CONTACT_COLUMN_USER_ONE + " = " + owner + " OR " +
                                DatabaseConstants.CONTACT_COLUMN_USER_TWO + " = " + owner;
                        connection.query(query, res -> {
                            if (res.succeeded()) {
                                output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                                ja.add(output);
                                for (int i = 0; i < res.result().getRows().size(); i++){
                                    JsonObject contactList = new JsonObject();
                                    contactList.put("id", res.result().getRows().get(i).getInteger("id"));
                                    contactList.put("creator", res.result().getRows().get(i).getInteger("creator"));
                                    contactList.put("user_one", res.result().getRows().get(i).getInteger("user_one"));
                                    contactList.put("user_two", res.result().getRows().get(i).getInteger("user_two"));
                                    contactList.put("confirmed", res.result().getRows().get(i).getBoolean("confirmed"));
                                    ja.add(contactList);
                                }
                                message.reply(ja);
                            } else {
                                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                                message.reply(output);
                            }

                        });
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
        this.future.complete();
    }

}
