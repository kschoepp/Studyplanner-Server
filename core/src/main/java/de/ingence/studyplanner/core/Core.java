package de.ingence.studyplanner.core;

import de.ingence.studyplanner.core.constants.AddressRegister;
import de.ingence.studyplanner.core.constants.MethodRegister;
import de.ingence.studyplanner.core.verticles.*;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.eventbus.Message;


public class Core extends AbstractVerticle {

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        //Getting EventBus from VERT.X instance
        EventBus eventBus = vertx.eventBus();

        //================================
        //=== Define EventBus Handlers ===
        //================================
        //|---Handler for UserClient
        eventBus.consumer(AddressRegister.GET_ALL_USER, message -> this.deployUserVerticle(MethodRegister.GET_ALL_USER, message));
        eventBus.consumer(AddressRegister.POST_USER_REGISTER, message -> this.deployUserVerticle(MethodRegister.POST_USER_REGISTER, message));
        eventBus.consumer(AddressRegister.DELETE_USER, message -> this.deployUserVerticle(MethodRegister.DELETE_USER, message));
        eventBus.consumer(AddressRegister.GET_USER_LOGIN, message -> this.deployUserVerticle(MethodRegister.GET_USER_LOGIN, message));
        eventBus.consumer(AddressRegister.PATCH_USER, message -> this.deployUserVerticle(MethodRegister.PATCH_USER, message));
        eventBus.consumer(AddressRegister.PATCH_USER_PASSWORD, message -> this.deployUserVerticle(MethodRegister.PATCH_USER_PASSWORD, message));
        eventBus.consumer(AddressRegister.PATCH_USER_ACTIVE, message -> this.deployUserVerticle(MethodRegister.PATCH_USER_ACTIVE, message));
        eventBus.consumer(AddressRegister.GET_USER_REGISTER_USERNAME, message -> this.deployUserVerticle(MethodRegister.GET_USER_REGISTER_USERNAME, message));
        eventBus.consumer(AddressRegister.GET_USER_DETAILS, message -> this.deployUserVerticle(MethodRegister.GET_USER_DETAILS, message));
        eventBus.consumer(AddressRegister.GET_USER_BY_ID, message -> this.deployUserVerticle(MethodRegister.GET_USER_BY_ID, message));
        eventBus.consumer(AddressRegister.GET_USER_BY_ACT_KEY, message -> this.deployUserVerticle(MethodRegister.GET_USER_BY_ACT_KEY, message));
        //|---Handler for TodoClient
        eventBus.consumer(AddressRegister.POST_TODO, message -> this.deployTodoVerticle(MethodRegister.POST_TODO, message));
        eventBus.consumer(AddressRegister.DELETE_TODO, message -> this.deployTodoVerticle(MethodRegister.DELETE_TODO, message));
        eventBus.consumer(AddressRegister.PATCH_TODO, message -> this.deployTodoVerticle(MethodRegister.PATCH_TODO, message));
        eventBus.consumer(AddressRegister.PATCH_TODO_DONE, message -> this.deployTodoVerticle(MethodRegister.PATCH_TODO_DONE, message));
        eventBus.consumer(AddressRegister.GET_TODO_BY_KEY, message -> this.deployTodoVerticle(MethodRegister.GET_TODO_BY_KEY, message));
        eventBus.consumer(AddressRegister.GET_TODO_BY_USER, message -> this.deployTodoVerticle(MethodRegister.GET_TODO_BY_USER, message));
        //|---Handler for ExamClient
        eventBus.consumer(AddressRegister.POST_EXAM, message -> this.deployExamVerticle(MethodRegister.POST_EXAM, message));
        eventBus.consumer(AddressRegister.DELETE_EXAM, message -> this.deployExamVerticle(MethodRegister.DELETE_EXAM, message));
        eventBus.consumer(AddressRegister.PATCH_EXAM, message -> this.deployExamVerticle(MethodRegister.PATCH_EXAM, message));
        eventBus.consumer(AddressRegister.GET_EXAM_BY_KEY, message -> this.deployExamVerticle(MethodRegister.GET_EXAM_BY_KEY, message));
        eventBus.consumer(AddressRegister.GET_EXAM_BY_USER, message -> this.deployExamVerticle(MethodRegister.GET_EXAM_BY_USER, message));
        //|---Handler for CourseClient
        eventBus.consumer(AddressRegister.POST_COURSE, message -> this.deployCourseVerticle(MethodRegister.POST_COURSE, message));
        eventBus.consumer(AddressRegister.DELETE_COURSE, message -> this.deployCourseVerticle(MethodRegister.DELETE_COURSE, message));
        eventBus.consumer(AddressRegister.PATCH_COURSE, message -> this.deployCourseVerticle(MethodRegister.PATCH_COURSE, message));
        eventBus.consumer(AddressRegister.GET_COURSE_BY_KEY, message -> this.deployCourseVerticle(MethodRegister.GET_COURSE_BY_KEY, message));
        eventBus.consumer(AddressRegister.GET_COURSE_BY_USER, message -> this.deployCourseVerticle(MethodRegister.GET_COURSE_BY_USER, message));
        //|---Handler for ContactClient
        eventBus.consumer(AddressRegister.POST_CONTACT, message -> this.deployContactVerticle(MethodRegister.POST_CONTACT, message));
        eventBus.consumer(AddressRegister.DELETE_CONTACT, message -> this.deployContactVerticle(MethodRegister.DELETE_CONTACT, message));
        eventBus.consumer(AddressRegister.PATCH_CONTACT_CONFIRM, message -> this.deployContactVerticle(MethodRegister.PATCH_CONTACT_CONFIRM, message));
        eventBus.consumer(AddressRegister.GET_CONTACT_BY_ID, message -> this.deployContactVerticle(MethodRegister.GET_CONTACT_BY_ID, message));
        eventBus.consumer(AddressRegister.GET_CONTACT_BY_USER, message -> this.deployContactVerticle(MethodRegister.GET_CONTACT_BY_USER, message));
        //|---Handler for CalendarClient
        eventBus.consumer(AddressRegister.POST_CALENDAR, message -> this.deployCalendarVerticle(MethodRegister.POST_CALENDAR, message));
        eventBus.consumer(AddressRegister.DELETE_CALENDAR, message -> this.deployCalendarVerticle(MethodRegister.DELETE_CALENDAR, message));
        eventBus.consumer(AddressRegister.PATCH_CALENDAR, message -> this.deployCalendarVerticle(MethodRegister.PATCH_CALENDAR, message));
        eventBus.consumer(AddressRegister.GET_CALENDAR_BY_KEY, message -> this.deployCalendarVerticle(MethodRegister.GET_CALENDAR_BY_KEY, message));
        eventBus.consumer(AddressRegister.GET_CALENDAR_BY_USER, message -> this.deployCalendarVerticle(MethodRegister.GET_CALENDAR_BY_USER, message));
        eventBus.consumer(AddressRegister.GET_CALENDAR_BY_DATE, message -> this.deployCalendarVerticle(MethodRegister.GET_CALENDAR_BY_DATE, message));

        startFuture.complete();
    }

    private void deployUserVerticle(int method, Message<Object> message) {
        vertx.deployVerticle(new UserClient(method, message), res -> {
            if(res.succeeded()) {
                vertx.undeploy(res.result());
            }
        });
    }

    private void deployTodoVerticle(int method, Message<Object> message) {
        vertx.deployVerticle(new TodoClient(method, message), res -> {
            if(res.succeeded()) {
                vertx.undeploy(res.result());
            }
        });
    }

    private void deployExamVerticle(int method, Message<Object> message) {
        vertx.deployVerticle(new ExamClient(method, message), res -> {
            if(res.succeeded()) {
                vertx.undeploy(res.result());
            }
        });
    }

    private void deployCourseVerticle(int method, Message<Object> message) {
        vertx.deployVerticle(new CourseClient(method, message), res -> {
            if(res.succeeded()) {
                vertx.undeploy(res.result());
            }
        });
    }

    private void deployContactVerticle(int method, Message<Object> message) {
        vertx.deployVerticle(new ContactClient(method, message), res -> {
            if(res.succeeded()) {
                vertx.undeploy(res.result());
            }
        });
    }

    private void deployCalendarVerticle(int method, Message<Object> message) {
        vertx.deployVerticle(new CalendarClient(method, message), res -> {
            if(res.succeeded()) {
                vertx.undeploy(res.result());
            }
        });
    }

    /**
     * OLD CORE
     //Deploying verticles
     //|---UserClient
     vertx.deployVerticle(new UserClient());
     //|---TodoClient
     vertx.deployVerticle(new TodoClient());
     //|---CourseClient
     vertx.deployVerticle(new CourseClient());
     //|---ExamClient
     vertx.deployVerticle(new ExamClient());
     //|---CalendarClient
     vertx.deployVerticle(new CalendarClient());
     //|---ContactClient
     vertx.deployVerticle(new ContactClient());
     **/

}
