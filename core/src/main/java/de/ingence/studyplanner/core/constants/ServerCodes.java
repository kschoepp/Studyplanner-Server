package de.ingence.studyplanner.core.constants;


public class ServerCodes {

    //UserClient-Messages
    public static final int USERNAME_EXISTS = 901;
    public static final int USER_EXISTS = 902;
    public static final int USERNAME_NOT_EXISTS = 903;
    public static final int PASSWORD_WRONG = 904;
    public static final int USER_AUTHORIZIED = 905;
    public static final int USERDATA_MISSING = 906;
    public static final int TODODATA_MISSING = 907;
    public static final int EXAMDATA_MISSING = 908;
    public static final int COURSEDATA_MISSING = 909;
    public static final int CALENDERDATA_MISSING = 910;
    public static final int CONTACTDATA_MISSING = 911;
}
