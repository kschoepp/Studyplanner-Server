package de.ingence.studyplanner.core.verticles;

import de.ingence.studyplanner.core.config.ConfigService;
import de.ingence.studyplanner.core.constants.*;
import io.vertx.core.AbstractVerticle;
import io.vertx.core.Future;
import io.vertx.core.eventbus.Message;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.asyncsql.PostgreSQLClient;
import io.vertx.ext.sql.SQLClient;
import io.vertx.ext.sql.SQLConnection;

import java.util.Calendar;
import java.util.Date;


public class CalendarClient extends AbstractVerticle {

    //Global variables
    private Future<Void> future;
    private int method;
    //|---VERT.X Objects
    private Message<Object> message;
    private SQLClient calendarClient;

    public CalendarClient(int method, Message<Object> message) {
        this.method = method;
        this.message = message;
    }

    @Override
    public void start(Future<Void> startFuture) throws Exception {
        this.future = startFuture;
        //Creating database client
        JsonObject clientConfig = ConfigService.getDatabaseConfig();
        this.calendarClient = PostgreSQLClient.createShared(vertx, clientConfig);

        switch (method) {
            //|---GET
            case MethodRegister.GET_CALENDAR_BY_KEY:
                this.getCalendarByKey();
                break;
            case MethodRegister.GET_CALENDAR_BY_USER:
                this.getCalendarByUser();
                break;
            case MethodRegister.GET_CALENDAR_BY_DATE:
                this.getCalendarEventByDate();
                break;
            //|---POST
            case MethodRegister.POST_CALENDAR:
                this.createCalendarEvent();
                break;
            //|---PATCH
            case MethodRegister.PATCH_CALENDAR:
                this.updateCalendarEvent();
                break;
            //|---DELETE
            case MethodRegister.DELETE_CALENDAR:
                this.deleteCalendarEvent();
                break;
            default:
                break;
        }
    }

    private void getCalendarEventByDate() {
        JsonArray output = new JsonArray();
        calendarClient.getConnection(con -> {
            if(con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                int owner = messageJson.getInteger("owner");
                long date = messageJson.getLong("date");
                Date curDate = new Date(date);
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(curDate);
                calendar.set(Calendar.HOUR_OF_DAY, 0);
                calendar.set(Calendar.MINUTE, 1);
                Date startDate = calendar.getTime();
                calendar.set(Calendar.HOUR_OF_DAY, 23);
                calendar.set(Calendar.MINUTE, 59);
                Date endDate = calendar.getTime();
                String query = "SELECT * FROM " + DatabaseConstants.TABLE_CALENDAR + " WHERE " +
                        DatabaseConstants.CALENDER_COLUMN_DATE_START + " >= " + startDate.getTime() + " AND " +
                        DatabaseConstants.CALENDER_COLUMN_DATE_END + " <= " + endDate.getTime() + " AND " +
                        DatabaseConstants.CALENDER_COLUMN_OWNER + " = " + owner + " ORDER BY " + DatabaseConstants.CALENDER_COLUMN_DATE_START + " ASC";
                connection.query(query, res -> {
                    if(res.succeeded()) {
                        output.add(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.OK));
                        for(int i = 0; i < res.result().getRows().size(); i++) {
                            JsonObject calendarEvent = new JsonObject();
                            calendarEvent.put("key", res.result().getRows().get(i).getString("key"));
                            calendarEvent.put("topic", res.result().getRows().get(i).getString("topic"));
                            calendarEvent.put("owner", res.result().getRows().get(i).getInteger("owner"));
                            calendarEvent.put("date_start", res.result().getRows().get(i).getLong("date_start"));
                            calendarEvent.put("date_end", res.result().getRows().get(i).getLong("date_end"));
                            calendarEvent.put("location", res.result().getRows().get(i).getString("location"));
                            calendarEvent.put("participants", res.result().getRows().get(i).getString("participants"));
                            output.add(calendarEvent);
                        }
                        message.reply(output);
                    } else {
                        output.add(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR));
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.add(new JsonObject().put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR));
                message.reply(output);
            }
        });
    }

    /**
     * Method to create a calendar event
     * ---------------------------------------------
     * EventBus-Address: POST_CALENDAR
     * ---------------------------------------------
     * Param:       JSON-Object with the following values
     *              "username":     String username
     *              "password":     String password
     *              "key":          String key
     *              "topic":        String topic
     *              "owner":        int id
     *              "date_start":   String date_start
     *              "date_end":     String date_end
     *              "time_start":   String time_start
     *              "time_end":     String time_end
     *              "location":     String location
     *              "participants"  String participants
     *              "created"       String created
     * ---------------------------------------------
     * Reply to EventBus:
     *              JSON-Object with the following values
     *              "httpCode":     int HTTP-Code
     *              "serverCode":   int Server-Code
     */
    private void createCalendarEvent() {
        JsonObject output = new JsonObject();
        calendarClient.getConnection(con -> {
            if(con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String key = messageJson.getString("key");
                String topic = messageJson.getString("topic");
                int owner = messageJson.getInteger("owner");
                long date_start = messageJson.getLong("date_start");
                long date_end = messageJson.getLong("date_end");
                if( topic == null){
                    output.put(CoreConstants.HTTP_CODE, HttpCodes.BAD_REQUEST);
                    output.put(CoreConstants.SERVER_CODE, ServerCodes.CALENDERDATA_MISSING);
                    message.reply(output);
                } else {
                    String location = messageJson.getString("location");
                    String participants = messageJson.getString("participants");
                    String query = "INSERT INTO" + DatabaseConstants.TABLE_CALENDAR + " (" +
                            DatabaseConstants.CALENDER_COLUMN_KEY + ", " +
                            DatabaseConstants.CALENDER_COLUMN_TOPIC + ", " +
                            DatabaseConstants.CALENDER_COLUMN_OWNER + ", " +
                            DatabaseConstants.CALENDER_COLUMN_DATE_START + ", " +
                            DatabaseConstants.CALENDER_COLUMN_DATE_END + ", " +
                            DatabaseConstants.CALENDER_COLUMN_CREATED;
                    if(location != null) { query += ", " + DatabaseConstants.CALENDER_COLUMN_LOCATION;}
                    if(participants != null){ query += ", " + DatabaseConstants.CALENDER_COLUMN_PARTICIPANTS;}
                    query += " ) VALUES (" +
                            "'" + key + "', '" + topic + "', " + owner + ", " + date_start + ", " + date_end + ", " + "NOW()";
                    if(location != null) {query += ", '" + location + "'";}
                    if(participants != null) {query += ", '" + participants + "'";}
                    query += " )";
                    connection.update(query, res -> {
                        if(res.succeeded()){
                            output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                            message.reply(output);
                            vertx.eventBus().send(AddressRegister.MQTT,
                                    new JsonObject().put("username",
                                            messageJson.getString("username")).put("component", "calendar").put("method", "post").put("key", key));
                        } else {
                            output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                            message.reply(output);
                        }
                    });
                }
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
    }

    /**
     * Method to delete a calendar event
     * ---------------------------------------------
     * EventBus-Address: DELETE_CALENDAR
     * ---------------------------------------------
     * Param:       JSON-Object with the following values
     *              "username":     String username
     *              "password":     String password
     *              "key":          String key
     * ---------------------------------------------
     * Reply to EventBus:
     *              JSON-Object with the following values
     *              "httpCode":     int HTTP-Code
     *              "serverCode":   int Server-Code
     */
    private void deleteCalendarEvent() {
        JsonObject output = new JsonObject();
        calendarClient.getConnection(con -> {
            if (con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String key = messageJson.getString("key");
                String query = "DELETE FROM " + DatabaseConstants.TABLE_CALENDAR + " WHERE " +
                        DatabaseConstants.CALENDER_COLUMN_KEY + " = '" + key + "'";
                connection.update(query, res-> {
                    if(res.succeeded()){
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                        vertx.eventBus().send(AddressRegister.MQTT,
                                new JsonObject().put("username",
                                        messageJson.getString("username")).put("component", "calendar").put("method", "delete").put("key", key));
                        message.reply(output);
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
    }


    /**
     * Method to update a calendar event
     * ---------------------------------------------
     * EventBus-Address: PATCH_CALENDAR
     * ---------------------------------------------
     * Param:       JSON-Object with the following values
     *              "username":     String username
     *              "password":     String password
     *              "key":          String key
     *              "topic":        String topic
     *              "owner":        int id
     *              "date_start":   String date_start
     *              "date_end":     String date_end
     *              "time_start":   String time_start
     *              "time_end":     String time_end
     *              "location":     String location
     *              "participants"  String participants
     *              "created"       String created
     * ---------------------------------------------
     * Reply to EventBus:
     *              JSON-Object with the following values
     *              "httpCode":     int HTTP-Code
     *              "serverCode":   int Server-Code
     */
    private void updateCalendarEvent() {
        JsonObject output = new JsonObject();
        calendarClient.getConnection(con -> {
            if (con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String key = messageJson.getString("key");
                String topic = messageJson.getString("topic");
                int owner = messageJson.getInteger("owner");
                long date_start = messageJson.getLong("date_start");
                long date_end = messageJson.getLong("date_end");
                if(topic == null) {
                    output.put(CoreConstants.HTTP_CODE, HttpCodes.BAD_REQUEST);
                    output.put(CoreConstants.SERVER_CODE, ServerCodes.CALENDERDATA_MISSING);
                    message.reply(output);
                }
                String location = messageJson.getString("location");
                String participants = messageJson.getString("participants");
                String query = "UPDATE " + DatabaseConstants.TABLE_CALENDAR + " SET " +
                        DatabaseConstants.CALENDER_COLUMN_TOPIC + " = '" + topic + "', " +
                        DatabaseConstants.CALENDER_COLUMN_OWNER + " = " + owner + ", " +
                        DatabaseConstants.CALENDER_COLUMN_DATE_START + " = " + date_start + ", " +
                        DatabaseConstants.CALENDER_COLUMN_DATE_END + " = " + date_end;
                if(location != null){query += ", " + DatabaseConstants.COURSE_COLUMN_LOCATION + " = '" + location + "'";}
                if(participants != null){query += ", " + DatabaseConstants.CALENDER_COLUMN_PARTICIPANTS + " = '" + participants + "'";}
                query += " WHERE " + DatabaseConstants.CALENDER_COLUMN_KEY + " = '" + key + "'";
                connection.update(query, res-> {
                    if(res.succeeded()){
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                        vertx.eventBus().send(AddressRegister.MQTT,
                                new JsonObject().put("username",
                                        messageJson.getString("username")).put("component", "calendar").put("method", "patch").put("key", key));
                        message.reply(output);
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
    }

    private void getCalendarByKey() {
        JsonObject output = new JsonObject();
        calendarClient.getConnection(con -> {
            if (con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String key = messageJson.getString("key");
                String query = "SELECT * FROM " + DatabaseConstants.TABLE_CALENDAR + " WHERE " +
                        DatabaseConstants.CALENDER_COLUMN_KEY + " = '" + key + "'";
                connection.query(query, res -> {
                    if (res.succeeded()) {
                        if(res.result().getRows().size() != 0) {
                            output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                            output.put("key", res.result().getRows().get(0).getString("key"));
                            output.put("topic", res.result().getRows().get(0).getString("topic"));
                            output.put("owner", res.result().getRows().get(0).getInteger("owner"));
                            output.put("date_start", res.result().getRows().get(0).getLong("date_start"));
                            output.put("date_end", res.result().getRows().get(0).getLong("date_end"));
                            output.put("location", res.result().getRows().get(0).getString("location"));
                            output.put("participants", res.result().getRows().get(0).getString("participants"));
                            output.put("created", res.result().getRows().get(0).getString("created"));
                            message.reply(output);
                        } else {
                            output.put(CoreConstants.HTTP_CODE, HttpCodes.BAD_REQUEST);
                            message.reply(output);
                        }
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        message.reply(output);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                message.reply(output);
            }
        });
    }

    private void getCalendarByUser() {
        JsonObject output = new JsonObject();
        JsonArray ja = new JsonArray();
        calendarClient.getConnection(con -> {
            if (con.succeeded()) {
                SQLConnection connection = con.result();
                JsonObject messageJson = (JsonObject) message.body();
                String username = messageJson.getString("username");
                String queryUsername = "SELECT id FROM " + DatabaseConstants.TABLE_USER + " WHERE " +
                        DatabaseConstants.USER_COLUMN_USERNAME + " = '" + username + "'";
                connection.query(queryUsername, resUsername -> {
                    if(resUsername.succeeded()) {
                        int owner = resUsername.result().getRows().get(0).getInteger("id");
                        String query = "SELECT * FROM " + DatabaseConstants.TABLE_CALENDAR + " WHERE " +
                                DatabaseConstants.CALENDER_COLUMN_OWNER + " = " + owner + "";
                        connection.query(query, res -> {
                            if (res.succeeded()) {
                                if(res.result().getRows().size() != 0) {
                                    output.put(CoreConstants.HTTP_CODE, HttpCodes.OK);
                                    ja.add(output);
                                    for (int i = 0; i < res.result().getRows().size(); i++) {
                                        JsonObject calendarList = new JsonObject();
                                        calendarList.put("key", res.result().getRows().get(i).getString("key"));
                                        calendarList.put("topic", res.result().getRows().get(i).getString("topic"));
                                        calendarList.put("owner", res.result().getRows().get(i).getInteger("owner"));
                                        calendarList.put("date_start", res.result().getRows().get(i).getLong("date_start"));
                                        calendarList.put("date_end", res.result().getRows().get(i).getLong("date_end"));
                                        calendarList.put("location", res.result().getRows().get(i).getString("location"));
                                        calendarList.put("participants", res.result().getRows().get(i).getString("participants"));
                                        calendarList.put("created", res.result().getRows().get(i).getString("created"));
                                        ja.add(calendarList);
                                    }
                                    message.reply(ja);
                                } else {
                                    output.put(CoreConstants.HTTP_CODE, HttpCodes.BAD_REQUEST);
                                    ja.add(output);
                                    message.reply(ja);
                                }
                            } else {
                                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                                ja.add(output);
                                message.reply(ja);
                            }

                        });
                    } else {
                        output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                        ja.add(output);
                        message.reply(ja);
                    }
                });
                connection.close();
            } else {
                output.put(CoreConstants.HTTP_CODE, HttpCodes.INTERNAL_SERVER_ERROR);
                ja.add(output);
                message.reply(ja);
            }
        });
    }

}
