package calendarClientTests;


import de.ingence.studyplanner.core.constants.AddressRegister;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;


public class UpdateCalendarTest {

    public static void main(String[] args) {
        try {
            //Creating VertxOptions
            VertxOptions options = new VertxOptions();
            options.setBlockedThreadCheckInterval(1000 * 60 * 60);
            Vertx vertx = Vertx.vertx(options);
//            vertx.deployVerticle(new CalendarClient(), ar -> {
//                if(ar.succeeded()) {
//                    //Demo data
//                    JsonObject demoData = new JsonObject();
//                    demoData.put("key", "345");
//                    demoData.put("topic", "Easter");
//                    demoData.put("owner", 2);
//                    demoData.put("date_start", "10.04.2018");
//                    demoData.put("date_end", "11.04.2018");
//                    demoData.put("time_start", "00:00");
//                    demoData.put("time_end", "00:00");
//                    demoData.put("created", "14.03.2016, 17:43:08");
//
//                    //Sending data to VERT.X EventBus
//                    EventBus eventBus = vertx.eventBus();
//
//                    eventBus.send(AddressRegister.PATCH_CALENDAR,demoData, res -> {
//                        if(res.succeeded()) {
//                            System.out.println("Received reply:");
//                            System.out.println(res.result().body().toString());
//                        } else if(res.failed()) {
//                            System.out.println(res.cause().toString());
//                        }
//                    });
//                }
//            });

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}


