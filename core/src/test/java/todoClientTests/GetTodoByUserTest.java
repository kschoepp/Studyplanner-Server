package todoClientTests;

import de.ingence.studyplanner.core.constants.AddressRegister;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;

/**
 * (C) Ingence
 * www.ingence.de
 * Created by Kathrin Schöpp on 06.03.2018
 **/
public class GetTodoByUserTest {

    public static void main(String[] args) {
        try {
            //Creating VertxOptions
            VertxOptions options = new VertxOptions();
            options.setBlockedThreadCheckInterval(1000 * 60 * 60);
            Vertx vertx = Vertx.vertx(options);
//            vertx.deployVerticle(new TodoClient(), ar -> {
//                if(ar.succeeded()) {
//                    //Demo data
//                    JsonObject demoData = new JsonObject();
//                    demoData.put("username", "hansus");
//
//                    //Sending data to VERT.X EventBus
//                    EventBus eventBus = vertx.eventBus();
//
//                    eventBus.send(AddressRegister.GET_TODO_BY_USER,demoData, res -> {
//                        if(res.succeeded()) {
//                            System.out.println("Received reply:");
//                            System.out.println(res.result().body().toString());
//                        } else if(res.failed()) {
//                            System.out.println(res.cause().toString());
//                        }
//                    });
//                }
//            });

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
