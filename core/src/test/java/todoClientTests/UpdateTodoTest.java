package todoClientTests;

import de.ingence.studyplanner.core.constants.AddressRegister;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;

/**
 * (C) Ingence
 * www.ingence.de
 * Created by Kathrin Schöpp on 06.03.2018
 **/
public class UpdateTodoTest {

    public static void main(String[] args) {
        try {
            //Creating VertxOptions
            VertxOptions options = new VertxOptions();
            options.setBlockedThreadCheckInterval(1000 * 60 * 60);
            Vertx vertx = Vertx.vertx(options);
//            vertx.deployVerticle(new TodoClient(), ar -> {
//                if(ar.succeeded()) {
//                    //Demo data
//                    JsonObject demoData = new JsonObject();
//                    demoData.put("key", "234");
//                    demoData.put("task", "Hausarbeit");
//                    demoData.put("owner", 6);
//                    demoData.put("priority", "eilt nicht");
//                    demoData.put("done", false);
//                    demoData.put("created", "15.06.2014");
//
//                    //Sending data to VERT.X EventBus
//                    EventBus eventBus = vertx.eventBus();
//
//                    eventBus.send(AddressRegister.PATCH_TODO,demoData, res -> {
//                        if(res.succeeded()) {
//                            System.out.println("Received reply:");
//                            System.out.println(res.result().body().toString());
//                        } else if(res.failed()) {
//                            System.out.println(res.cause().toString());
//                        }
//                    });
//                }
//            });

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
