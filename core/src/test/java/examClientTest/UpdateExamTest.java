package examClientTest;

import de.ingence.studyplanner.core.constants.AddressRegister;
import io.vertx.core.Vertx;
import io.vertx.core.VertxOptions;
import io.vertx.core.eventbus.EventBus;
import io.vertx.core.json.JsonObject;

/**
 * (C) Ingence
 * www.ingence.de
 * Created by Kathrin Schöpp on 06.03.2018
 **/
public class UpdateExamTest {

    public static void main(String[] args) {
        try {
            //Creating VertxOptions
            VertxOptions options = new VertxOptions();
            options.setBlockedThreadCheckInterval(1000 * 60 * 60);
            Vertx vertx = Vertx.vertx(options);
//            vertx.deployVerticle(new ExamClient(), ar -> {
//                if(ar.succeeded()) {
//                    //Demo data
//                    JsonObject demoData = new JsonObject();
//                    demoData.put("key", "235");
//                    demoData.put("title", "Prog");
//                    demoData.put("owner", 1);
//                    demoData.put("date", "29.03.2018");
//                    demoData.put("time_start", "15:00");
//                    demoData.put("time_end", "17:00" );
//                    demoData.put("location", "CAS");
//
//                    //Sending data to VERT.X EventBus
//                    EventBus eventBus = vertx.eventBus();
//
//                    eventBus.send(AddressRegister.PATCH_EXAM,demoData, res -> {
//                        if(res.succeeded()) {
//                            System.out.println("Received reply:");
//                            System.out.println(res.result().body().toString());
//                        } else if(res.failed()) {
//                            System.out.println(res.cause().toString());
//                        }
//                    });
//                }
//            });

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
